========================================================================
       WIN32 APPLICATION : AutoInstall
========================================================================


AppWizard has created this AutoInstall application for you.  

This file contains a summary of what you will find in each of the files that
make up your AutoInstall application.

AutoInstall.cpp
    This is the main application source file.

AutoInstall.dsp
    This file (the project file) contains information at the project level and
    is used to build a single project or subproject. Other users can share the
    project (.dsp) file, but they should export the makefiles locally.
	

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named AutoInstall.pch and a precompiled types file named StdAfx.obj.


/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.


/////////////////////////////////////////////////////////////////////////////
