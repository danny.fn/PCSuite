//Common.h
#ifndef	__GPRS_COMMON_H__
#define	__GPRS_COMMON_H__

#define GUOYW_DEBUG 0

#include "atlbase.h"
//#include <afxdb.h>
//#include <odbcinst.h>
//#include "atlbase.h"

//Parameters
//CString m_strAPN,m_strUser,m_strPass;
#define _DEBUG_VERSION_ TRUE

#define MS_PRINT(szOutPut)  if(_DEBUG_VERSION_ == TRUE)\
                            TRACE(szOutPut)
                            

//OS Version
typedef enum tagVersion_Os Version_Os;  
enum tagVersion_Os
{
    OS_WIN98 = 1,   OS_WIN2000, 
    OS_WINXP,       OS_OTHERS    
};

#define OS_WIN98	1
#define OS_WIN2000	2
#define OS_WINXP	3
#define OS_OTHERS	4

#define WM_TRAY_NOTIFY WM_USER + 1000 // ADD BY SHAOZW
#define TIMER_MODEM_DEVICE_CONNECTED   10
#define	TIMER_OPEN_COM					1
#define TIMER_DETECT_CONNECTION_STATUS 2
#define TIMER_SPEED_CHANGE				3
#define	TIMEE_RELEASE_TMEP				4
#define TIMER_START_QUEST				5
#define	TIMER_CAN_DIALUP				6
#define TIMER_DIALUP					7
#define DETECT_CONNECTION_ELAPSE       1000
//define Timer ID
#define ID_EVENT_REFRESH 1
#define ID_EVENT_DIALSTATUS 2
#define ID_EVENT_DETECTDIAL 3
#define ID_EVENT_SETUPPROCESS 4
#define ID_EVENT_DELAYCOMMAND 5
#define ID_EVENT_PROMPT_5S 6
#define ID_EVENT_KILLPROMPT 7
#define ID_EVENT_DETECTCONN 8
			
#define ID_START_REFRESHURL 9
#define ID_TEST_STATUS 10

#define ID_ENUMUSBCOM		1
#define ID_ENUMSERIALCOM	2

#define ID_ENUMCOM		3
#define ID_ENUMMODEM	3

//define Common Value
//#define MAX_URL 100

//define message
#define WM_RAS_GETTINGDIALSTATUS		WM_USER+1
#define WM_COMM_CTS_DETECTED			WM_USER+2	// The CTS (clear-to-send) signal changed state. 
#define WM_COMM_RXCHAR					WM_USER+3	// A character was received and placed in the input buffer. 

#define ERROR_SetupDiClassNameFromGuid			7001L
#define ERROR_SetupDiCreateDeviceInfoList		7002L
#define ERROR_SetupDiCreateDeviceInfo			7003L
#define ERROR_SetupDiGetDeviceInstanceId		7004L
#define ERROR_SetupDiGetDeviceInstallParams		7005L
#define ERROR_GetFileAttributes					7006L
#define ERROR_SetupDiSetDeviceInstallParams		7007L
#define ERROR_SetupDiBuildDriverInfoList		7008L
#define ERROR_SetupDiSetSelectedDriver			7009L
#define ERROR_SetupDiRegisterDeviceInfo			7010L
#define ERROR_SetupDiRegisterCoDeviceInstallers	7011L
#define ERROR_SetupDiCallClassInstaller			7012L
#define ERROR_RegisterModemCOM					7013L


// 设置串口号
#define COM1							1
#define COM2							2
#define COM3							3
#define COM4							4
#define COM5							5
#define COM6							6
#define COM7							7
#define COM8							8
#define COM9							9
#define COM10							10
#define COM11							11
#define COM12							12
#define COM13							13
#define COM14							14
#define COM15							15
#define COM16							16
#define COM17							17
#define COM18							18
#define COM19							19
#define COM20							20


#define AMOI_NET_SETTING            _T("Software\\AMOI WP3.0")
#define AMOI_SYS_SETTING            _T("Software\\AMOI WP3.0\\SysSeting")
#define AMOI_INIT_APN               "AT+CGDCONT=1,\"IP\",\"tre.it\""
//#define _ERROR_DEBUG_
typedef enum _USBCOM_TYPE_Tag
{
	USBCOM = 1,
	SERIALCOM = 2,
	UNKNOWNCOM = 3

}USBCOM_TYPE;


//定义扩展对话框的结构体，这一结构在MFC里没有定义，但是在MSDN里可以找到
#ifndef	__REMOVE_WINDOWSXP_DRIVER_SIGNING__
#define	__REMOVE_WINDOWSXP_DRIVER_SIGNING__

#define	titleLen	MAX_PATH	// 因为我们只用到title,所以这一长度随便定义一个，
								// 不要太短就可以了

typedef	WORD	sz_Or_Ord;		// 经过试验得出在这里sz_Or_Ord为两个字节宽，至于为什么
								// 是两个字节宽我也不知道

extern bool   gModemConnect;							
extern bool	 n_Check1Value;
extern int 	ConnectionSet;
extern int 	MainDlgStatue;

//extern bool   gPreConnectState;
typedef struct 
{  
	WORD      dlgVer; 
	WORD      signature; 
	DWORD     helpID; 
	DWORD     exStyle; 
	DWORD     style; 
	WORD      cDlgItems; 
	short     x; 
	short     y; 
	short     cx; 
	short     cy; 
	sz_Or_Ord menu; 
	sz_Or_Ord windowClass; 
	WCHAR     title[titleLen]; 
	// The following members exist only if the style member is 
	// set to DS_SETFONT or DS_SHELLFONT.
} DLGTEMPLATEEX, * PDLGTEMPLATEEX, * LPDLGTEMPLATEEX; 


typedef enum
{
	STATUS_IDLE,
	STATUS_BEGINCONNECT,
	STATUS_CONNECT,
	STATUS_DISCONNECTING
}TDialStatus;

// 取应用程序目录
static void GetAppRootDir(CString &str)
{
    TCHAR szFileName[_MAX_PATH] = _T("");
    GetModuleFileName(NULL, szFileName, _MAX_PATH);
    TCHAR *p = _tcschr(szFileName, L'\\');
    if (p)
        *(p + 1) = 0;

    str.Format(L"%s", szFileName);
}

// 取得操作系统版本，估计也用不到了
static int  GetOSVersion()
{
    int nOSVersion = OS_OTHERS;
    
    OSVERSIONINFO OsVersionInfo;
    OsVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx(&OsVersionInfo);
    if (OsVersionInfo.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
        nOSVersion = OS_WIN98;
    else if (OsVersionInfo.dwPlatformId == VER_PLATFORM_WIN32_NT)
    {
        TCHAR szVersion[256]=L"";
        DWORD	dwSize = 256L;
        CRegKey keyVersion;
        CString strVersion;
        
        LONG lResult = keyVersion.Open(HKEY_LOCAL_MACHINE,
            L"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion");
        if (lResult == ERROR_SUCCESS)
        {
            LONG lResultQuery = keyVersion.QueryValue(szVersion,L"ProductName",&dwSize);
            if (lResult == ERROR_SUCCESS)
            {
                strVersion = szVersion;
                if (strVersion.Find(L"2000") != -1)
                    nOSVersion = OS_WIN2000;
                else
                    nOSVersion = OS_WINXP;
            }
            keyVersion.Close();
        }
    }
    
    return nOSVersion;
}

// 在桌面和开始的程序栏创建快捷方式，估计也不用了
static BOOL CreateDeskLnk(TCHAR* pLinkName, TCHAR* pModulName)
{
    IShellLink *MyLink;
    IPersistFile *ppf;
    TCHAR LinkLocate[MAX_PATH];
    TCHAR DesktopLocate[MAX_PATH];
    
    SHGetSpecialFolderPath(NULL,&LinkLocate[0],CSIDL_PROGRAMS,0);
    _stprintf(LinkLocate + _tcslen(LinkLocate), L"\\%s.LNK", pLinkName);
    
    SHGetSpecialFolderPath(NULL,&DesktopLocate[0],CSIDL_DESKTOP,0);
    _stprintf(DesktopLocate + _tcslen(DesktopLocate), L"\\%s.LNK", pLinkName);
    
    CoInitialize(NULL);
    int re = CoCreateInstance(CLSID_ShellLink,NULL,CLSCTX_ALL, IID_IShellLink,(void **)&MyLink); 
    if (re < 0) 
        return FALSE; 
    
    CString strPathName, strFileName;
    GetAppRootDir(strPathName);
    strFileName.Format(L"%s\\%s", strPathName, pModulName);
    MyLink->SetPath(strFileName);                                   //设置快捷方式实际文件位置 
    MyLink->SetDescription(pLinkName);                              //设置快捷方式描述(即名称) 
    MyLink->SetWorkingDirectory(strPathName);
    re=MyLink->QueryInterface(IID_IPersistFile, (void **)&ppf); 
    if (re < 0) 
    { 
        MyLink->Release(); 
        return FALSE; 
    } 
    
   // WCHAR wsz[MAX_PATH]; 
   // MultiByteToWideChar( CP_ACP,0,LinkLocate,-1,wsz,MAX_PATH);      //转换为unicode 
    ppf->Save(LinkLocate, true);                                           //在开始菜单创建快捷方式 
    
   // MultiByteToWideChar(CP_ACP,0,DesktopLocate,-1,wsz,MAX_PATH);    //转换为unicode 
    ppf->Save(DesktopLocate, true);                                           //在桌面创建快捷方式 
    
    ppf->Release(); 
    MyLink->Release();
    
    return TRUE;
}

// 读注册表
static BOOL ReadRegister(CString strKeyName, CString strValueName, CString &strValue)
{
    CRegKey regSubKey;
    if (regSubKey.Open(HKEY_LOCAL_MACHINE, strKeyName) != ERROR_SUCCESS)
	{
		regSubKey.Close();
        return FALSE;
	}

    DWORD dwLen = 256L;
    if (regSubKey.QueryValue(/*(LPSTR)(LPCTSTR)*/strValue.GetBuffer(100),  // MODIFIED BY SHAOZW
        (LPCTSTR)strValueName, &dwLen) != ERROR_SUCCESS)
	{   
		strValue.ReleaseBuffer(); // ADD BY SHAOZW (really curious! :-O)
		regSubKey.Close();
        return FALSE;
	}
    
	strValue.ReleaseBuffer(); // ADD BY SHAOZW
	regSubKey.Close();
    return TRUE;
}

// 写注册表
static BOOL WriteRegister(CString strKeyName, CString strValueName, CString strValue)
{
    CRegKey regSubKey;
    if (regSubKey.Create(HKEY_LOCAL_MACHINE, strKeyName) != ERROR_SUCCESS)
        return FALSE;
    
    if (regSubKey.SetValue(strValue, strValueName) != ERROR_SUCCESS)
        return FALSE;
    
    if (regSubKey.Close() != ERROR_SUCCESS)
        return FALSE;
    
    return TRUE;
}

// 删除键值
static BOOL DeleteRegister(CString strKeyName, CString strValueName)
{
    CRegKey regSubKey;
    if (regSubKey.Open(HKEY_LOCAL_MACHINE, strKeyName) != ERROR_SUCCESS)
        return FALSE;
    
    if (regSubKey.DeleteValue(strValueName) != ERROR_SUCCESS)
        return FALSE;
    
    return TRUE;
}

static BOOL AddFone()
{
	CString szFile,szPath;
	int i;
	
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\Resource\\h3g.ttf");

	i = AddFontResource(szFile);	   //安装字体文件到windows系统
	::SendMessage(HWND_BROADCAST,WM_FONTCHANGE,0,0);  //通知其他app更新界面 

	return TRUE;
}

#endif //__REMOVE_WINDOWSXP_DRIVER_SIGNING__

#endif //__GPRS_COMMON_H__
