#pragma once
#include "afxwin.h"
#include "RgnDialog.h"
#include "StaticTrans.h"
#include "ExtendButton.h"

// CConnectionDlg 对话框

class CConnectionDlg : public CRgnDialog
{
	//DECLARE_DYNAMIC(CConnectionDlg)

public:
	CConnectionDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CConnectionDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_CONNECTION };

private:
	CBitmap  m_Bitmap;
	
protected:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC );
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheck1();
	CButton m_check1;
	bool n_Check2Value;
	bool n_Check3Value;
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck3();
	CString GetText(UINT uID);
	CButton m_Check2;
	CButton m_Check3;
	CStaticTrans m_Title;
	CStaticTrans m_checktext1;
	CStaticTrans m_checktext2;
	CStaticTrans m_checktext3;
	CButton m_bOK;
	CButton m_BCancel;
	
public:
	afx_msg void OnBnClickedButton1();

public:
	afx_msg void OnBnClickedButton2();

};
