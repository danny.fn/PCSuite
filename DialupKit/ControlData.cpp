// ControlData.cpp: implementation of the CControlData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <atlbase.h>	

#include "Common.h"
#include "RasEntryAmoi.h"
#include "ControlData.h"
#include "DialSetDlg.h"
#include "HangingupStatusDlg.h"
#include "StatusDlg.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include <atlbase.h>		//GUO ADD
#include <direct.h>
#include <setupapi.h>
#include "RegistryEx.h"
#include "RS232.h"
#include "SetupAPI.h"
#include <devguid.h>

#define AMOI_MOBILE_MODEM	L"INQ1 USB Modem"


UINT Current_OSVersion = OS_OTHERS;

#define TIMER_MODEM_DEVICE_CONNECTED   10
#define TIMER_DETECT_CONNECTION_STATUS 2
#define DETECT_CONNECTION_ELAPSE       1000
HWND    ghWnd;

CString m_strInitKey;
static USBCOM_TYPE m_USBCOMType = UNKNOWNCOM;
bool   gModemConnect = FALSE;
//bool   gPreConnectState = TRUE;

static UINT AmoiRasHangUp( LPVOID pParam );
void SleepAfterHangUp(HRASCONN hrasConn);
static VOID WINAPI RasDialFunc( UINT unMsg, RASCONNSTATE rasconnstate, DWORD dwError );

static DIAL_PRAM DEF_DialPara[] =
{
	{ _T("china"),_T("CMNET"),_T(""),  _T(""),_T("*99#") ,_T("") ,_T("")},
	{ _T(""),_T(""),_T(""),  _T(""),_T("#777") ,_T("") ,_T("")}
};

static TCHAR* Header[] = { _T("Country"), _T("APN"), _T("UserName"),_T("Password") ,_T("DialNo"),_T("IP"),_T("homepage")};
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SINGLETON(CControlData);

// 初始化一些默认参数
void CControlData::InitSettings() 
{

	// MODIFIED BY SHAOZW (WE ACTUALLY NEED ONLY 3 PARAMETERS(USER NAME, PASSWORD, PHONE NUMBER)
    int nDefPram = ((CDialupKitApp*)AfxGetApp())->m_bNetType;

	//m_pDialPram = &DEF_DialPara[nDefPram];
	CopyParam(&DEF_DialPara[nDefPram],((CDialupKitApp*)AfxGetApp())->m_pDialPram);	
    
	WriteNetSettings();

    m_hRasConn = NULL;
    m_pStatusDlg = NULL;
}

// 初始化一些其他变量
void CControlData::InitVariable() 
{
	m_hRasConn = NULL; // ADD BY SHAOZW
    m_pStatusDlg = NULL; // ADD BY SHAOZW
    m_bConnectFlag = FALSE;
}

/*
 *	初始化(如果没有查询到网络设置，需要用默认参数)
 */
BOOL CControlData::InitSystem(HWND hWnd)
{
    BOOL bResult = FALSE;

    m_hWnd = hWnd;
    ghWnd = hWnd;
	
	((CDialupKitApp*)AfxGetApp())->m_pDialPram  = new DIAL_PRAM;

	// query settings from registry, if failed, initialize
   if (!QueryNetSettings())
        InitSettings();
    
    InitVariable();



	// ADD BY GUOYW
	//if(QueryCOM())
	{
		//SysSetup();
	}
	
	//安装调制解调器
	//if (!QueryModem())
	{
		//PolicySet(FALSE);
		//InstallSuntekModem(m_CurrentCOM);
		//PolicySet(TRUE);
	}

    return bResult;
}

// 检测Modem的驱动是否存在，这个可以不用了
BOOL CControlData::DetectModemDrive()
{
    CRasEntryAmoi RasEntry;
    if (!RasEntry.ExistModemDriver())
        return FALSE;

    return TRUE;
}

// 检测当前PC是被列表中是否有夏新手机的Modem连接，带回的引用是设备名，要用到的
BOOL CControlData::DetectModemConnect(CString &strDevice)
{
    CRasEntryAmoi RasEntry;
	
	//gModemConnect = TRUE;
   if (m_USBCOMType != SERIALCOM&&!RasEntry.EnumModem(RASDT_Modem, strDevice))
   	{
       TRACE(L"No find modem device");
	   //gModemConnect = FALSE;
	   //return FALSE;
   	}
   TRACE(L"Find modem device");
    // add by shaozw
    m_strDeviceName = strDevice;
    return TRUE;
}


BOOL CControlData::Is8512ModemConnect()        //detect the checked if curent device is 8512 modem
{
    gModemConnect =  QueryCOM();

    return gModemConnect;
}

// 在"网络邻居"中建立连接图标
BOOL CControlData::EsTablishLinkAttribute(CString strDevice)
{
    RASENTRY entryRAS;
//    entryRAS.dwfOptions = RASEO_UseCountryAndAreaCodes;
    _tcscpy(entryRAS.szDeviceType, RASDT_Modem);

	DIAL_PRAM* pDialPram= ((CDialupKitApp*)AfxGetApp())->m_pDialPram;
    // 都需要用变量代替
//    entryRAS.dwCountryID = RASDT_COUNTRYID;
//    entryRAS.dwCountryCode = RASDT_COUNTRYCODE;
//    strcpy(entryRAS.szAreaCode, RASDT_AREACODE);
//    strcpy(entryRAS.szLocalPhoneNumber, RASDT_LOCALPHONENUMBER); 

    _tcscpy(entryRAS.szDeviceName, strDevice);
    
    CRasEntryAmoi RasEntry;
	//user the current modem to create an entry
    if (!RasEntry.CreateRasEntry(RASDT_LINKNAME_CM, entryRAS))
        return FALSE;

    // 建立好连接图标后，设置Pbk目录下的rasphone.pbk文件参数
    SetPbkProfileParam();

	// MODIFIED BY SHAOZW(CHANGE CONSTANT TO VARIABLE)
    // RasEntry.SetEntryDialParams(RASDT_LINKNAME_CM, AMOI_DEFAULVALUE_USERNAM, 
    // AMOI_DEFAULVALUE_PASSWORD, FALSE);
	//pDialPram->szPhoneNumber =L"*99#";
	RasEntry.SetEntryDialParams(RASDT_LINKNAME_CM, 
                                pDialPram->szUserName, 
                                pDialPram->szPassword, 
                                pDialPram->szPhoneNumber, 
                                FALSE);
    
    return TRUE;
}

// 建立好连接图标后，设置Pbk目录下的rasphone.pbk文件参数
BOOL CControlData::SetPbkProfileParam()
{
    CRasEntryAmoi RasEntry;
//    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_BASEPROTOCOL, 1);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_EXCLUDEDPROTOCOLS, 0);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_SWCOMPRESSION, 0);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_DIALPERCENT, 75);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_DIALSECONDS, 120);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_HANGUPPERCENT, 10);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_HANGUPSECONDS, 120);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_SHAREMSFILEPRINT, 0);
//RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_BINDMSNETCLIENT, 0);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_PREVIEWUSERPW, 1);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_SHOWDIALINGPROGRESS, 1);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_SHOWICONINTASKBAR, 1);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_MS_MSCLIENT, 1);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_MS_SERVER, 0);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_HWFLOWCONTROL, 0);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_PROTOCOL, 0);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_COMPRESSION, 0);
    RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, PBKPROFILE_SPEAKER, 0);


	RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, "LcpExtensions", 0);
	RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, "NegotiateMultilinkAlways", 0);
	RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, "RedialSeconds", 60);
	RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, "RedialAttempts", 3);
	RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, "DialMode", 1);
	RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, "SharedPhoneNumbers", 1);
	RasEntry.WritePbkProfile(RASDT_LINKNAME_CM, "ms_psched", 1);
    return TRUE;
}

// 弹出设置对话框，设置C网、G网参数，并用WriteNetSettings()写入注册表
// (当前只完成C网，这个函数最后需要能设置C网、G网参数)
BOOL CControlData::SetGPRS()
{
  CDialSetDlg dlg;
  //dlg.m_szAPN	= GetParamByIndex(1);
  //TRACE(dlg.m_szAPN);
  //dlg.m_szUser	= GetParamByIndex(2);  
  //TRACE(dlg.m_szUser);
  //dlg.m_szPass	= GetParamByIndex(3);
  //TRACE(dlg.m_szPass);
  //dlg.m_szNO	= GetParamByIndex(4);
  //TRACE(dlg.m_szNO);
  //dlg.m_szIP	= GetParamByIndex(5);
  //TRACE(dlg.m_szIP);

 
	//if (dlg.DoModal() != IDOK)
	//return FALSE;

	if (!SysSetup())
	  return FALSE;
	CDialupKitApp *pApp = ((CDialupKitApp*)AfxGetApp());

	pApp->m_pDialPram->szCountry = _tcsdup(dlg.m_szCountry);
	pApp->m_pDialPram->szAPN = _tcsdup(dlg.m_szAPN);
	pApp->m_pDialPram->szUserName =_tcsdup(dlg.m_szUser);
	pApp->m_pDialPram->szPassword =_tcsdup(dlg.m_szPass);
	pApp->m_pDialPram->szPhoneNumber =_tcsdup(dlg.m_szNO);
	pApp->m_pDialPram->szIPAddress =_tcsdup(dlg.m_szIP);
	
	WriteNetSettings();	
    return TRUE;
}

// 查询网络设置(APN, UserName, Password, PhoneNumber)
BOOL CControlData::QueryNetSettings()
{
	int nCount = _countof(Header);
	CStringArray  strArr;
	CString szFile,szPath;
	TCHAR Return[255] = {0};

	//写ini
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");
	
	/*for(int n=0;n<nCount;n++)
	{
		CString szTmp;
		if (!ReadRegister(AMOI_NET_SETTING,Header[n], szTmp))
			return FALSE;
		strArr.Add(szTmp);
	}*/
	
	CDialupKitApp *pApp = ((CDialupKitApp*)AfxGetApp());
	GetPrivateProfileString(L"DialupSet",L"Country",L"1",Return,255,szFile);
	pApp->m_pDialPram->szCountry = _tcsdup(Return);
	GetPrivateProfileString(L"DialupSet",L"APN",L"1",Return,255,szFile);
	pApp->m_pDialPram->szAPN = _tcsdup(Return);
	GetPrivateProfileString(L"DialupSet",L"UserName",L"1",Return,255,szFile);
	pApp->m_pDialPram->szUserName =_tcsdup(Return);
	GetPrivateProfileString(L"DialupSet",L"Password",L"1",Return,255,szFile);
	pApp->m_pDialPram->szPassword =_tcsdup(Return);
	GetPrivateProfileString(L"DialupSet",L"DialNo",L"1",Return,255,szFile);
	pApp->m_pDialPram->szPhoneNumber =_tcsdup(Return);
	GetPrivateProfileString(L"DialupSet",L"IP",L"1",Return,255,szFile);
	pApp->m_pDialPram->szIPAddress =_tcsdup(Return);
	GetPrivateProfileString(L"DialupSet",L"homepage",L"1",Return,255,szFile);
	pApp->m_pDialPram->szHomepage =_tcsdup(Return); 

    return TRUE;
}

// 写入网络设置(APN, UserName, Password, PhoneNumber)
BOOL CControlData::WriteNetSettings()
{
   
	int nCount = _countof(Header);
	CString szFile,szPath;

	//写ini
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");
	
	/*for(int n=0;n<nCount;n++)
	{
		if (!WriteRegister(AMOI_NET_SETTING,Header[n], GetParamByIndex(n)))
		return FALSE;
	}*/
   	WritePrivateProfileString(L"DialupSet",L"Country",GetParamByIndex(0),szFile);
   	WritePrivateProfileString(L"DialupSet",L"APN",GetParamByIndex(1),szFile);
   	WritePrivateProfileString(L"DialupSet",L"UserName",GetParamByIndex(2),szFile);
   	WritePrivateProfileString(L"DialupSet",L"Password",GetParamByIndex(3),szFile);
   	WritePrivateProfileString(L"DialupSet",L"DialNo",GetParamByIndex(4),szFile);
   	WritePrivateProfileString(L"DialupSet",L"IP",GetParamByIndex(5),szFile);
   	WritePrivateProfileString(L"DialupSet",L"homepage",GetParamByIndex(6),szFile);
	
    return TRUE;
}

VOID WINAPI RasDialFunc( UINT unMsg, RASCONNSTATE rasconnstate, DWORD dwError )
{
    PostMessage(ghWnd, WM_RAS_GETTINGDIALSTATUS, (WPARAM)rasconnstate,(LPARAM)dwError);
}

// 发起拨号
BOOL CControlData::BeginConnect()
{
	
	if(IsConnectionActive())
	{
		return FALSE;
	}
	else
	{
		m_bConnectFlag = FALSE;
	}
    
	// hung up connections if necessary
	if(m_hRasConn != NULL)
	{
		RASCONNSTATUS rasConnStatus;
		if (0 == RasGetConnectStatus(m_hRasConn, &rasConnStatus))
		{
			if (RASCS_Disconnected != rasConnStatus.rasconnstate)
				StopConnect();
		}
		else
		{
			m_hRasConn = NULL;
		}
	}
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
    // 获取参数指定的拨号连接的用户名以及密码等参数
    RASDIALPARAMS DialParam;
    DialParam.dwSize = sizeof(RASDIALPARAMS);
    _tcscpy(DialParam.szEntryName, RASDT_LINKNAME_CM);   // 或者 RASDT_LINKNAME_GM
    
    BOOL b;
    int ret = RasGetEntryDialParams(NULL, &DialParam, &b);

    lstrcpy(DialParam.szCallbackNumber, _T(""));

	//lstrcpy(DialParam.szUserName, m_strUserName);
    //lstrcpy(DialParam.szPhoneNumber, _T("*99#"));
    //lstrcpy(DialParam.szUserName, m_strUserName);
    //lstrcpy(DialParam.szPassword, m_strPassword);
    TRACE(DialParam.szEntryName);
    TRACE(L"\n");
    TRACE(DialParam.szPhoneNumber);
    TRACE(L"\n");
    TRACE(DialParam.szUserName);
    TRACE(L"\n");
    TRACE(DialParam.szPassword);
    TRACE(L"\n");

    // RasDial参数很有讲究，调用前一定保证m_hRasConn为NULL，否则可能会进入到“!!! occur error”代码中
    //establishes a RAS connection 
    int nResponse = RasDial(NULL, NULL, &DialParam, 0, &RasDialFunc, &m_hRasConn);
    if (nResponse)	//!!! occur error
    {
        CString szMsg;
        if(RasGetErrorString((UINT)nResponse, szMsg.GetBuffer(500), 500) != 0)
        {
            // get ras error string error
            szMsg.ReleaseBuffer();
			// MODIFIED BY SHAOZW, CHANGE CHINESE TO ENGLISH
			//szMsg.Format(IDS_UNKNOWN_RAERROR, nResponse);
			szMsg=pApp->g_GetText(IDS_UNKNOWN_RAERROR,pApp->m_nLandId);
            //szMsg.Format("未知的远程访问错误，错误代码为：%ld.",nResponse);
        }
        else  // get ras error string successfully
            szMsg.ReleaseBuffer();
       
        // ADD 2005-12-05
        if(szMsg.IsEmpty())
        {
			szMsg=pApp->g_GetText(IDS_UNKNOWN_RAERROR,pApp->m_nLandId);
        }
        
        CString buf;
		// MODIFIED BY SHAOZW, CHANGE CHINESE TO ENGLISH
		buf=pApp->g_GetText(IDS_ERRORCODE,pApp->m_nLandId);
		buf.Format(buf, nResponse);
        //buf.Format("错误代码为：%ld.",nResponse);
        
        szMsg += buf;
        
		// MODIFIED BY SHAOZW, CHANGE CHINESE TO ENGLISH
		CString strCaption;

		strCaption = pApp->g_GetText(IDS_DIALING_FAILUREDLG_CAPTION,pApp->m_nLandId);
		//MessageBox(m_hWnd, szMsg, strCaption, MB_OK | MB_ICONSTOP);
         
        m_hRasConn = NULL;
    }

    m_bConnectFlag = nResponse ? FALSE : TRUE;

    return m_bConnectFlag;
}


typedef struct 
{
    LPHRASCONN lphRasConn;
	HANDLE   hEvent;
	BOOL*    pbHungUp;
}
PARAMS, *PPARAMS;

UINT AmoiRasHangUp(LPVOID pParam)  // ADD BY SHAOZW
{
    PPARAMS pParamStruct = (PPARAMS)pParam;

	
    if(RasHangUp((*pParamStruct->lphRasConn))) // hang up error
	{
        SleepAfterHangUp(*pParamStruct->lphRasConn);
        *pParamStruct->pbHungUp = FALSE;
	}

	SleepAfterHangUp(*pParamStruct->lphRasConn);
	
    *pParamStruct->lphRasConn = NULL;
	*pParamStruct->pbHungUp = TRUE;

	SetEvent(pParamStruct->hEvent);
    return 1;
}





// 停止拨号
BOOL CControlData::StopConnect()
{
	// MODIFIED BY SHAOZW TO STOP CONNECTION WHICH IS NOT CREATED BY OUR TOOL
	//    if(m_bConnectFlag != TRUE)
	//        return FALSE;
	
	if(!IsConnectionActive(&m_hRasConn) && m_hRasConn == NULL) // no connection is active
	{
		return FALSE;
	}
    
 
	
	
	m_bConnectFlag = FALSE;
	
	
	HANDLE  hEvent;
    hEvent  =  CreateEvent(NULL,FALSE,FALSE,NULL); //创建手动重置的Event对象 
	BOOL   bHungup;
    PARAMS params;
	params.hEvent = hEvent;
	params.pbHungUp = &bHungup;
	params.lphRasConn = &m_hRasConn;
	
	AfxBeginThread(AmoiRasHangUp, &params);
	
	WaitForSingleObject(hEvent, INFINITE);
	//CHangingupStatusDlg statusDlg(hEvent);
	//statusDlg.DoModal();  // wait for the thread to terminate
	
	return *params.pbHungUp;
}

// 拨号后产生的各种呼叫状态
LRESULT CControlData::GetDialStatus(WPARAM wParam, LPARAM lParam, CString &strInfo)
{
  
BOOL bRet = FALSE;
	
	// MODIFIED BY SHAOZW
	static CString strStatus;  // ADD static 2005-11-23 to remember last tip string(display the last string if the string wasn't changed)

	CString str; // tmp string

	if(NULL == m_hRasConn)
		return 0;
	
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
    // CString strStatus("!");
    switch((RASCONNSTATE)wParam)
    {
    case RASCS_OpenPort:
        //strStatus = IDS_OPENPORT;
        strInfo = pApp->g_GetText(T_OPENPORT,pApp->m_nLandId);
        break;
    case RASCS_PortOpened:
        strInfo = pApp->g_GetText(T_PORTOPENED,pApp->m_nLandId);
        break;
    case RASCS_ConnectDevice:
        strInfo = pApp->g_GetText(T_CONNECTDEVICE,pApp->m_nLandId);
        break;
    case RASCS_DeviceConnected:
        strInfo = pApp->g_GetText(T_DEVICECONNECTED,pApp->m_nLandId);
        break;
//    case RASCS_AllDevicesConnected:   // ADD BY SHAOZW
//        strStatus.LoadString(T_ALLDEVICESCONNECTED);
//		break;
    case RASCS_Authenticate:
        strInfo = pApp->g_GetText(T_AUTHENTICATE,pApp->m_nLandId);
        break;

    case RASCS_Authenticated:
        strInfo = pApp->g_GetText(T_AUTHENTICATED,pApp->m_nLandId);
        break;
    case RASCS_AuthProject:  // ADD BY SHAOZW
        strInfo = pApp->g_GetText(T_AUTHPROJECT,pApp->m_nLandId);
		break;

    case RASCS_Connected:
        //strStatus = IDS_CONNECTED;
        //strInfo = pApp->g_GetText(T_USERDO3,pApp->m_nLandId);

		strInfo = pApp->g_GetText(T_USERDO4,pApp->m_nLandId);

		// ADD BY SHAOZW TO SAVE PHONE NUMBER
		/*&{
			RASENTRY RasEntry;
			DWORD dwEntryInfoSize = 0;
			memset((void*)&RasEntry, 0, sizeof(RASENTRY));
			RasEntry.dwSize = sizeof(RASENTRY);
			
			// To determine the required buffer size
            DWORD ret0 = RasGetEntryProperties(NULL, RASDT_LINKNAME_CM, NULL, &dwEntryInfoSize, NULL, NULL);
			DWORD ret1 = RasGetEntryProperties(NULL, RASDT_LINKNAME_CM, &RasEntry, &dwEntryInfoSize, NULL, NULL);
		    
			_tcscpy(RasEntry.szLocalPhoneNumber, (LPTSTR)(LPCTSTR)m_strPhoneNumber);

			// save phone number
			DWORD ret2 = RasSetEntryProperties(NULL, RASDT_LINKNAME_CM, &RasEntry, dwEntryInfoSize, NULL, NULL);
		}*/


		// set timer to detect connection status
		SetTimer(ghWnd, TIMER_DETECT_CONNECTION_STATUS, DETECT_CONNECTION_ELAPSE, NULL);
		
		m_bConnectFlag = FALSE;

        break;
    case RASCS_Disconnected:
       	strInfo = pApp->g_GetText(T_LIST_GPRS,pApp->m_nLandId);
        m_hRasConn = NULL;
        m_bConnectFlag = FALSE;
        break;
    default:
        break;
    }

    if ((DWORD)lParam)
    {
       // TCHAR buf[MAX_PATH];
        //ZeroMemory(buf,sizeof(buf));
        //RasGetErrorString((DWORD)lParam, buf, sizeof(buf));
        //strStatus.Format(IDS_ERRORCODE_MESSAGE, (DWORD)lParam, buf);

		m_bConnectFlag = FALSE;
        StopConnect();
		//strInfo = pApp->g_GetText(T_USERDO2,pApp->m_nLandId);
		
		//SetRASStatus((DWORD)lParam, strStatus);
		SetTimer(ghWnd, TIMER_CAN_DIALUP, 15000, NULL);
    }
    // added by shaozw for debug
	static int cnt = 1;
	TRACE(L"Execute CControlData::GetDialStatus : %d\n", cnt++);

    TRACE(L"%s\n", strInfo);
    
    return 0; 
}

#define CLOSE_STATUS_DLG_ELAPSE  5000
// 显示拨号后各种状态信息
void CControlData::SetRASStatus(DWORD dwErrorCode, CString strInfo)
{
   // m_pStatusDlg = new CStatusDlg(strInfo);     // 这个带参数的构造函数是为了显示拨号后的第一条信息
	
	if(STR_HANGINGUP == strInfo) // hanging up now
	{
		//		m_pStatusDlg->GetDlgItem(IDOK)->ShowWindow(SW_HIDE);
		//		m_pStatusDlg->GetDlgItem(IDCANCEL)->ShowWindow(SW_HIDE);
		//		m_pStatusDlg->SetTimer(IDTIMER_CLOSE_STATUS_DLG, CLOSE_STATUS_DLG_ELAPSE, NULL);
	}
	static int cnt = 1;
    m_pStatusDlg->DoModal();
    delete m_pStatusDlg;
    m_pStatusDlg = NULL;
 
}

/************************************************************************/
/*  安装 Suntek Modem(以下代码用处还不名，可能G网会用到)                */
/************************************************************************/
static const GUID GUID_MODEM = {0x4D36E96D,0xE325,0x11CE,{0xBF,0xC1,0x08,0x00,0x2B,0xE1,0x03,0x18}};
int CControlData::InstallSuntekModem(LPCTSTR lpszComName)
{ 
    int ret = ERROR_SUCCESS;
    TCHAR szInfPath[ MAX_PATH ] = { 0 };
    TCHAR szInstanceID[ MAX_PATH ] = { 0 };
    TCHAR szHardwareID[ MAX_PATH ] = { 0 };
    TCHAR szDeviceDesc[ MAX_PATH ] = { 0 };
    TCHAR buffer[1024];
    
    HKEY hKey = NULL;
    HINF hInf = NULL;
    INFCONTEXT hContext;

    _tcscpy(szInfPath , L"c:\\amoi-gprs\\AMOI.inf");
    
    hInf = SetupOpenInfFile(szInfPath , NULL , INF_STYLE_WIN4 , NULL);
    if(hInf != INVALID_HANDLE_VALUE)
    { 
        if(SetupFindFirstLine(hInf , L"Manufacturer" , NULL , &hContext))
        { 
            if(SetupGetStringField(&hContext , 1 , buffer , sizeof(buffer) , NULL))
            { 
                if(SetupFindFirstLine(hInf , buffer , NULL , &hContext))
                {
                    //获得设备描述
                    SetupGetStringField(&hContext , 0 , szDeviceDesc , sizeof(szDeviceDesc) , NULL);
                    //获得HardwareID
                    SetupGetStringField(&hContext , 2, szHardwareID , sizeof(szHardwareID) , NULL);
                }
            }
        }

        SetupCloseInfFile(hInf);

       if((ret = InstallRootEnumeratedDriver((LPGUID)&GUID_MODEM , szHardwareID , 
            szDeviceDesc, szInfPath , lpszComName, szInstanceID , NULL))!=ERROR_SUCCESS)
           return ret;
        
		if (!WriteRegister(AMOI_NET_SETTING,AMOI_VALUENAME_MODEM, szDeviceDesc))
			return FALSE;
        if(hKey!=NULL)
        {
            RegDeleteValue(hKey , L"MDMCOM");
            RegCloseKey(hKey);
        }
    }

    return ret;
}
int DisplayError(TCHAR * ErrorName)
{
    DWORD Err = GetLastError();
    LPVOID lpMessageBuffer = NULL;
	
    if (FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        Err,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMessageBuffer,
        0,
        NULL ))
        NULL;  //_tprintf(TEXT("%s FAILURE: %s\n"),ErrorName,(TCHAR *)lpMessageBuffer);
    else
        NULL;  //_tprintf(TEXT("%s FAILURE: (0x%08x)\n"),ErrorName,Err);
	
    if (lpMessageBuffer) LocalFree( lpMessageBuffer ); // Free system buffer
	
    SetLastError(Err);
    return FALSE;
}

BOOL FindExistingDevice(IN LPTSTR HardwareId) //查找是否存在改硬件ID
{
    HDEVINFO DeviceInfoSet;
    SP_DEVINFO_DATA DeviceInfoData;
    DWORD i,err;
    BOOL Found;

    //
    // Create a Device Information Set with all present devices.
    //
    DeviceInfoSet = SetupDiGetClassDevs(NULL, 0, 0,DIGCF_ALLCLASSES | DIGCF_PRESENT ); // All devices present on system

    if (DeviceInfoSet == INVALID_HANDLE_VALUE)
    {
        return DisplayError(TEXT("GetClassDevs(All Present Devices)"));
    }

    //_tprintf(TEXT("Search for Device ID: [%s]\n"),HardwareId);

    //
    //  Enumerate through all Devices.
    //
    Found = FALSE;
    DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
    for (i=0;SetupDiEnumDeviceInfo(DeviceInfoSet,i,&DeviceInfoData);i++)
    {
        DWORD DataT;
        LPTSTR p,buffer = NULL;
        DWORD buffersize = 0;

        //
        // We won't know the size of the HardwareID buffer until we call
        // this function. So call it with a null to begin with, and then
        // use the required buffer size to Alloc the nessicary space.
        // Keep calling we have success or an unknown failure.
        //
        while (!SetupDiGetDeviceRegistryProperty(
            DeviceInfoSet,
            &DeviceInfoData,
            SPDRP_HARDWAREID,
            &DataT,
            (PBYTE)buffer,
            buffersize,
            &buffersize))
        {
            if (GetLastError() == ERROR_INVALID_DATA)
            {
                //
                // May be a Legacy Device with no HardwareID. Continue.
                //
                break;
            }
            else if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
            {
                //
                // We need to change the buffer size.
                //
                if (buffer)
                    LocalFree(buffer);
                buffer = (TCHAR *)LocalAlloc(LPTR,buffersize);
            }
            else
            {
                //
                // Unknown Failure.
                //
                DisplayError(TEXT("GetDeviceRegistryProperty"));
                goto cleanup_DeviceInfo;
            }
        }

        if (GetLastError() == ERROR_INVALID_DATA)
            continue;

        //
        // Compare each entry in the buffer multi-sz list with our HardwareID.


        //
        for (p=buffer;*p&&(p<&buffer[buffersize]);p+=lstrlen(p)+sizeof(TCHAR))


        {
            //_tprintf(TEXT("Compare device ID: [%s]\n"),p);

            if (!_tcscmp(HardwareId,p))
            {
                //_tprintf(TEXT("Found! [%s]\n"),p);
                Found = TRUE;
                break;
            }
        }

        if (buffer) LocalFree(buffer);
        if (Found) break;
    }

    if (GetLastError() != NO_ERROR)
    {
        DisplayError(TEXT("EnumDeviceInfo"));
    }

    //
    //  Cleanup.
    //
cleanup_DeviceInfo:
    err = GetLastError();
    SetupDiDestroyDeviceInfoList(DeviceInfoSet);
    SetLastError(err);

    return err == NO_ERROR; //???
}

int CControlData::InstallRootEnumeratedDriver(
                                                IN LPGUID lpGUID,
	                                            IN LPCTSTR lpszHardwareID ,
	                                            IN LPCTSTR lpszDescription,
	                                            IN LPCTSTR lpszInfName,
	                                            IN LPCTSTR lpszComName,
	                                            OUT LPTSTR lpszInstanceID,
	                                            OUT LPBOOL pfNeedReboot
	                                         )
{
	int ret = ERROR_SUCCESS;
	DWORD dwEnumIndex;
	TCHAR szClassName[100];

	HDEVINFO hdi = NULL;
	SP_DEVINFO_DATA hDev = { 0 } ;
	SP_DEVINSTALL_PARAMS hInstParam = { 0 } ;
	SP_DRVINFO_DATA hDrv = { 0 } ;
	hDev.cbSize = sizeof(SP_DEVINFO_DATA);
	hInstParam.cbSize = sizeof(SP_DEVINSTALL_PARAMS);
	hDrv.cbSize = sizeof(SP_DRVINFO_DATA);

	try
	{
		//检测设备是否已经安装
	//	if(FindExistingDevice(lpGUID , lpszHardwareID , lpszInstanceID))
	//		return ERROR_SUCCESS;

		if(!SetupDiClassNameFromGuid(lpGUID , szClassName , sizeof(szClassName) ,NULL))
			throw(ERROR_SetupDiClassNameFromGuid);

		if((hdi = SetupDiCreateDeviceInfoList(lpGUID , NULL))==NULL)
			throw(ERROR_SetupDiCreateDeviceInfoList);

		//创建新设备信息。如果设备已经存在，有异常产生 
		if(!SetupDiCreateDeviceInfo(hdi , szClassName, lpGUID , lpszDescription, NULL , DICD_GENERATE_ID , &hDev))
			throw(ERROR_SetupDiCreateDeviceInfo);

		if(!SetupDiGetDeviceInstanceId(hdi , &hDev ,lpszInstanceID , MAX_PATH , NULL))
			throw(ERROR_SetupDiGetDeviceInstanceId);

		//获得设备安装参数
		if(!SetupDiGetDeviceInstallParams(hdi , &hDev , &hInstParam))
			throw(ERROR_SetupDiGetDeviceInstallParams);

		//修改参数
		if(lpszInfName!=NULL && _tcslen(lpszInfName)!=0)
		{
			DWORD fattr = GetFileAttributes(lpszInfName);
			if(fattr == -1)
				throw(ERROR_GetFileAttributes);

			if(!(fattr & FILE_ATTRIBUTE_DIRECTORY))
				hInstParam.Flags = hInstParam.Flags | DI_ENUMSINGLEINF;
			_tcscpy(hInstParam.DriverPath , lpszInfName);
		} 
		hInstParam.FlagsEx = hInstParam.FlagsEx | DI_FLAGSEX_ALLOWEXCLUDEDDRVS;

		//设置设备安装参数
		if(!SetupDiSetDeviceInstallParams(hdi, &hDev , &hInstParam))
			throw(ERROR_SetupDiSetDeviceInstallParams);

		//获得设备驱动信息列表
		if(!SetupDiBuildDriverInfoList(hdi, &hDev , SPDIT_CLASSDRIVER))
			throw(ERROR_SetupDiBuildDriverInfoList);

		//获得设备的驱动程序
		dwEnumIndex = 0;
		while(SetupDiEnumDriverInfo(hdi , &hDev , SPDIT_CLASSDRIVER , 
		dwEnumIndex++ , &hDrv))
		{
			if(_tcsicmp(hDrv.Description , lpszDescription)==0)
				break;
		}

		//选定这个驱动程序
		if(!SetupDiSetSelectedDriver(hdi , &hDev , &hDrv))
			throw(ERROR_SetupDiSetSelectedDriver);

		if (lpszComName==L"")
			AfxMessageBox(L"未检测到USB数据线串口！");

		if (!RegisterModem(hdi, &hDev, lpszComName) )
		{
			//Remove = TRUE;
			throw(ERROR_RegisterModemCOM);
		}

		//安装设备
		if(!SetupDiCallClassInstaller(DIF_INSTALLDEVICE , hdi , &hDev))
			throw(ERROR_SetupDiCallClassInstaller);

		if(pfNeedReboot!=NULL)
		{
			if(SetupDiGetDeviceInstallParams(hdi , &hDev , &hInstParam))
				*pfNeedReboot = ((hInstParam.Flags & DI_NEEDRESTART) 
					|| (hInstParam.Flags & DI_NEEDREBOOT));
			else
				pfNeedReboot = FALSE;
		}

	}
	catch(int e)
	{
		ret = e;
	}

	if(hdi!=NULL)
		SetupDiDestroyDeviceInfoList(hdi);

	return ret;
}

BOOL CControlData::RegisterModem(IN HDEVINFO hdi, 
                                 IN PSP_DEVINFO_DATA pdevData, 
	                             IN LPCTSTR pszPort)
{
	BOOL bRet;
	SP_DRVINFO_DATA drvData;
	DWORD nErr = NO_ERROR;
	DWORD dwRet;
	TCHAR const c_szAttachedTo[] = _T("AttachedTo");
	TCHAR const c_szUserInit[] = _T("UserInit");
	HKEY hKeyDev;
	bRet = FALSE;
    LPCTSTR pszIPV4= L"AT+CGDCONT=1,\"IP\",\"orange.fr\"";
	TCHAR szIPV4[ 255 ] = { 0 };
	lstrcpy(szIPV4, pszIPV4);
	lstrcat(szIPV4, ((CDialupKitApp*)AfxGetApp())->m_pDialPram->szAPN);

	if (m_strIPAddress.IsEmpty())
		lstrcat(szIPV4,L"\",,0,0");
	else
	{
		lstrcat(szIPV4,L"\",\"");
		lstrcat(szIPV4, m_strIPAddress);
		lstrcat(szIPV4,L"\",0,0");
	}
	pszIPV4 = (LPCTSTR)szIPV4;

	if( !SetupDiRegisterDeviceInfo(hdi, pdevData, 0, NULL, NULL, NULL) )
	{
		//DisplayError("Register Device 1\n");
		return bRet;
	}

	hKeyDev = SetupDiOpenDevRegKey(hdi, pdevData, DICS_FLAG_GLOBAL, 0, DIREG_DRV, KEY_ALL_ACCESS); //// This call fails....

	if( (INVALID_HANDLE_VALUE == hKeyDev) && ( ERROR_KEY_DOES_NOT_EXIST == GetLastError()) )
	{
		hKeyDev = SetupDiCreateDevRegKey(hdi, pdevData, DICS_FLAG_GLOBAL, 0, DIREG_DRV, NULL, NULL);

		if( INVALID_HANDLE_VALUE == hKeyDev )
		{
			//DisplayError(_T("SetupDi Open+Create DevRegKey failed: "));
			return FALSE;
		}
	}
	else 
	{
		//DisplayError(_T("Can not open DriverKey: "));
		return FALSE;
	}

	if (ERROR_SUCCESS != (dwRet = RegSetValueEx (hKeyDev, c_szAttachedTo, 0, REG_SZ,
		(PBYTE)pszPort, (lstrlen(pszPort)+1)*sizeof(TCHAR))))
	{
		//DisplayError(_T("RegSetValueEx: AttachedTo"));
		SetLastError (dwRet);
		bRet = FALSE;
	}
	
	//AT+CGDCONT=1,"IPV4","CMNET",,0,0
	if (ERROR_SUCCESS != (dwRet = RegSetValueEx (hKeyDev, c_szUserInit, 0, REG_SZ,
		(PBYTE)pszIPV4, (lstrlen(pszIPV4)+1)*sizeof(TCHAR))))
	{
		//DisplayError(_T("RegSetValueEx: IPV4"));
		SetLastError (dwRet);
		bRet = FALSE;
	}
	RegCloseKey (hKeyDev);

	if( !SetupDiRegisterDeviceInfo(hdi, pdevData, 0, NULL, NULL, NULL) )
	{
		//DisplayError(_T("Register Device 2"));
		return FALSE;
	}

	if ( !SetupDiGetSelectedDriver(hdi, pdevData, &drvData)) 
	{
		//DisplayError(_T("SetupDiGetSelectedDriver Failed: "));
	}

	return TRUE;
}


void SleepAfterHangUp(HRASCONN hrasConn) // ADD BY SHAOZW
{

	RASCONNSTATUS rasConnStatus;
	memset(&rasConnStatus, 0, sizeof(RASCONNSTATUS));


	// Before calling the function RasGetConnectStatus(), an application must set 
	// the dwSize member of the structure to sizeof(RASCONNSTATUS) in order to  
	// identify the version of the structure being passed. 
	rasConnStatus.dwSize = sizeof(RASCONNSTATUS);
	
    while(ERROR_INVALID_HANDLE != RasGetConnectStatus(hrasConn, &rasConnStatus))
	{
		Sleep(0);
	}
}

// if the connection is active and the phRasConn parameter is not NULL, 
// the handle of the connection is returned by phRasConn pointer
BOOL CControlData::IsConnectionActive(HRASCONN* phRasConn) // ADD BY SHAOZW
{
	RASCONNSTATUS rasConnStatus;
	memset(&rasConnStatus, 0, sizeof(RASCONNSTATUS));
	rasConnStatus.dwSize = sizeof(RASCONNSTATUS);
	
    RasGetConnectStatus(m_hRasConn, &rasConnStatus);

	// in most cases, if the connection created by our tool is acitve,just return true
	// to save time
	if(RASCS_Connected == rasConnStatus.rasconnstate)
	{
		return TRUE;
	}


	// 
	// enum all active RAS connections to check whether there is a active connection which was not created
	// by our tool but used the same device(WINVER >= 0x400) or the same phone-book entry(WINVER < 0x400)
	//
	LPRASCONN lpRasConn = NULL;
	DWORD dwcb = sizeof(RASCONN);
	DWORD dwConnections = 0;
    DWORD dwRet = 0;

	lpRasConn = (LPRASCONN)malloc(sizeof(RASCONN));
	lpRasConn[0].dwSize = sizeof(RASCONN);
	
	dwRet = RasEnumConnections(lpRasConn,  &dwcb,  &dwConnections);
    if (dwRet == ERROR_BUFFER_TOO_SMALL && dwConnections != 0)
    {
        //  found  more  then  one  connection(s)
        lpRasConn = (LPRASCONN)realloc((VOID*)lpRasConn, (UINT)dwcb);
		lpRasConn[0].dwSize = sizeof(RASCONN);

        dwRet = RasEnumConnections(lpRasConn, &dwcb, &dwConnections);
    }
	else
	{
		if(0 == dwConnections)
        {
            free(lpRasConn);
		    return FALSE;
        }
	}

	// Get Windows version
	BOOL bIsWindowsNTorLater = FALSE;
	OSVERSIONINFO osvi;
	memset((void*)&osvi, 0, sizeof(OSVERSIONINFO));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx (&osvi);

    bIsWindowsNTorLater = 
   (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT) &&
   ( (osvi.dwMajorVersion > 4) ||
   ( (osvi.dwMajorVersion == 4) && (osvi.dwMinorVersion > 0) ) );

	// iterate the  RASCONN array to find the entry name or device name depending on WINVER
	for(int i = 0; i < dwConnections; i++)
	{
		if(!bIsWindowsNTorLater)
		{
		    if(0 == _tcscmp(lpRasConn[i].szEntryName, RASDT_LINKNAME_CM)) // found it!
			{
				if(phRasConn != NULL) 
				{
					*phRasConn = lpRasConn[i].hrasconn;
				}

                free(lpRasConn);
			    return TRUE;
			}
		}
		else  // Windows NT or later
		{
		    if(0 == _tcscmp(lpRasConn[i].szDeviceName, m_strDeviceName))
			{
				if(phRasConn != NULL) 
				{
					*phRasConn = lpRasConn[i].hrasconn;
				}

                free(lpRasConn);
				return TRUE;
			}
		}

	}
	
    free(lpRasConn);
	return FALSE;
}


BOOL CControlData::IsPhoonBookEntryExist() // ADD BY SHAOZW
{
    CRasEntryAmoi RasEntry;
	return RasEntry.IsEntryExist(RASDT_LINKNAME_CM);

}





void CControlData::CopyParam(DIAL_PRAM* pSrc,DIAL_PRAM* pDst)
{
	pDst->szCountry =_tcsdup(pSrc->szCountry);
	pDst->szAPN=_tcsdup(pSrc->szAPN);
	pDst->szUserName=_tcsdup(pSrc->szUserName);
	pDst->szPassword=_tcsdup(pSrc->szPassword);
	pDst->szPhoneNumber=_tcsdup(pSrc->szPhoneNumber);
	pDst->szIPAddress=_tcsdup(pSrc->szIPAddress);
	pDst->szHomepage=_tcsdup(pSrc->szHomepage); 
}


CString CControlData::GetParamByIndex(int nIndex)
{

	CDialupKitApp *pApp = ((CDialupKitApp*)AfxGetApp());
	
	CString szRet;
	switch(nIndex) 
	{
	case 0 :
		szRet = pApp->m_pDialPram->szCountry;
		break;
	case 1 :
		szRet = pApp->m_pDialPram->szAPN;
		break;
	case 2:
		szRet = pApp->m_pDialPram->szUserName;
		break;
	case 3 :
		szRet = pApp->m_pDialPram->szPassword;
		break;
	case 4:
		szRet = pApp->m_pDialPram->szPhoneNumber ;
		break;
	case 5:
		szRet = pApp->m_pDialPram->szIPAddress;
		break;
	case 6:
		szRet = pApp->m_pDialPram->szHomepage;
		break;
	default:
			break;
	}
	return szRet;
}

BOOL CControlData::SysSetup()
{
	CRegKey regKey;
	CString keyName=AMOI_SYS_SETTING;
	if (regKey.Open(HKEY_LOCAL_MACHINE,keyName)==ERROR_SUCCESS)
	{
		TCHAR buffer[256]=_T("");
		DWORD strlen = 256L;
		
		if (regKey.QueryValue(buffer,AMOI_VALUENAME_COM,&strlen)!=ERROR_SUCCESS)
			return FALSE;
		m_RegCOM = buffer;
		
		strlen = 256L;
		if (regKey.QueryValue(buffer,AMOI_VALUENAME_MODEM,&strlen)!=ERROR_SUCCESS)
			return FALSE;
		m_RegModem = buffer;
		
		strlen = 256L;
		if (regKey.QueryValue(buffer,AMOI_VALUENAME_DIAL,&strlen)!=ERROR_SUCCESS)
			return FALSE;
		else
		{
			m_RegDial = buffer;
			
			if((m_RegCOM.Find(AMOI_DEFAULVALUE_COM)!=-1)
				&&(m_RegModem.Find(AMOI_DEFAULVALUE_MODEM)!=-1)
				&&(m_RegDial.Find(AMOI_DEFAULVALUE_DIAL)!=-1))
				m_bInstall = TRUE;
			
			return TRUE;
		}
	}
	else
		return FALSE;
}

// ADD BY GUOYW BEGIN

//////////////////////////////////////////////////////////////////////////

//获取操作系统版本

void CControlData::m_GetOSVersion()
{
	OSVERSIONINFO OsVersionInfo;
	
	OsVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&OsVersionInfo);
	
	if (OsVersionInfo.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
	{
		Current_OSVersion = OS_WIN98;
	}
	else if (OsVersionInfo.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		TCHAR szVersion[256]=_T("");
		DWORD	dwSize = 256L;
		CRegKey keyVersion;
		CString strVersion;

		LONG lResult = keyVersion.Open(
			HKEY_LOCAL_MACHINE,
			L"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion");

		if (lResult == ERROR_SUCCESS)
		{
			LONG lResultQuery = keyVersion.QueryValue(szVersion,L"ProductName",&dwSize);
			if (lResult == ERROR_SUCCESS)
			{
				//AfxMessageBox(szVersion);
				strVersion = szVersion;
				if (strVersion.Find(L"2000") != -1)
					Current_OSVersion = OS_WIN2000;
				else
					Current_OSVersion = OS_WINXP;
			}
			keyVersion.Close();
		}
	}
}

//////////////////////////////////////////////////////////////////////////

//数字签名

void CControlData::PolicySet(BOOL bFlag)
{
	m_GetOSVersion();
	//Win2000下去除数字签名
	//HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Driver Signing\Policy
	if(Current_OSVersion == OS_WIN2000)
	{
		CString keyName = _T("SOFTWARE\\Microsoft\\Driver Signing");

		BYTE lpData[4]; 
		HKEY hKey;
		DWORD dwDisposition;
		DWORD dwSize,dataType;

		if(RegCreateKeyEx(HKEY_LOCAL_MACHINE,keyName,NULL,L"Policy",REG_OPTION_NON_VOLATILE,
			KEY_ALL_ACCESS,NULL,&hKey,&dwDisposition) == ERROR_SUCCESS )
		{
			RegQueryValueEx(
				 hKey, 
				 L"Policy", // value name 
				 0, // reserved 
				 &dataType, // value type 
				 lpData, // value data 
				 &dwSize // size of value data 
				 );

			if (bFlag)
				lpData[0] = 1;
			else
				lpData[0] = 0;

			lpData[1] = lpData[2] = lpData[3] = 0;

			RegSetValueEx( 
				 hKey, 
				 L"Policy", // value name 
				 0, // reserved 
				 REG_BINARY, // value type 
				 (BYTE*)lpData, // value data 
				 4 // size of value data 
				 );

			RegCloseKey(hKey);
		}
		else
		{
			//AfxMessageBox("写注册表(去除数字签名)失败！");
		}
		return;
	}
}

//
const CString CControlData::GetUSBRegistrySubKey( const HKEY hKey, const CString strSection ) const
{
	CString strResult;

	CRegistryEx RegistryEx;
	if ( RegistryEx.Open( hKey, L"" ) )
	{
		CStringArray aKeys;
		if ( RegistryEx.ListKey( strSection, aKeys ) )
		{
			int nCount;
			const int nSize = aKeys.GetSize();
			for ( nCount = 0; nCount < nSize && strResult.IsEmpty(); nCount++ )
			{

				CString strKey = CString( strSection ) + L"\\" + aKeys[ nCount ] + L"\\" + L"Device Parameters";

				CString strKey2 = RegistryEx.FindKey(strKey, L"PortName", m_TempCOM );

				if ( !strKey2.IsEmpty() )
				{
					strResult = m_TempCOM;
					break;
				}
			}
		}
		RegistryEx.Close();
	}
		
	return strResult;
}

const CString CControlData::GetModemRegistrySubKey( const HKEY hKey, const CString strSection ) const
{
	CString strResult;
	CString strKey;

	CRegistryEx RegistryEx;
	if ( RegistryEx.Open( hKey, L"" ) )
	{
		CStringArray aKeys;
		if ( RegistryEx.ListKey( strSection, aKeys ) )
		{
			int nCount;
			const int nSize = aKeys.GetSize();
			for ( nCount = 0; nCount < nSize && strResult.IsEmpty(); nCount++ )
			{
				strKey = CString( strSection ) + L"\\" + aKeys[ nCount ] ;

				//CString strKey2 = RegistryEx.FindKey(strKey, L"DriverDesc", L"White Phone Proprietary USB Modem"/* L"Amoi-A500 Proprietary USB Modem" */);
				CString strKey3 = RegistryEx.FindKey(strKey, L"AttachedTo", m_CurrentCOM );

				//if ( (!strKey2.IsEmpty())&&(!strKey3.IsEmpty()) )
				if ( !strKey3.IsEmpty() )
				{
					//TRACE(strKey2);
					//TRACE(strKey2);
					strResult = MyQueryKeyValue(HKEY_LOCAL_MACHINE,strKey,L"FriendlyName");
					m_strInitKey = strKey;
					TRACE(strKey);
					//m_CurrentModem = strKey;
					//AT+CGDCONT=1,"IPV4","CMNET",,0,0
					break;
				}
			}
		}
		RegistryEx.Close();
	}

	return strResult;
}

const CString CControlData::GetEnumIndex( const int nIndex, const int nID ) const
{
	CString strIndex;
	strIndex.Format( L"%d", nIndex );
#define QC_USB_SER_FORMAT L"\\Device\\QCUSB_COM%d_1"
#define HERE_SERIAL L"\\Device\\Serial0"
	if (nID == 1)
	{
		CString str;
		str.Format(QC_USB_SER_FORMAT, nIndex);
		TRACE(L"GetEnumIndex\n\t\t");
		TRACE(str);
		TRACE(L"\n");
		return str;
	}
	else if (nID == 2)
	{
		ASSERT(0);
		return L"ERROR";//CString( HERE_SERIAL ).Left( strlen(HERE_SERIAL) - strIndex.GetLength() ) + strIndex; //SERIALCOM
	}
	else if (nID == 3)
		return CString( L"0000" ).Left( 4 - strIndex.GetLength() ) + strIndex;
	else
		return L"Error";
}

const CString CControlData::MyQueryKeyValue( const HKEY hKey, LPCTSTR pszKey, LPCTSTR pszValue ) const
{
	CString str;
	CRegKey regKey;
	TCHAR buffer[256]=_T("");
	DWORD strlen = 256L;
	TRACE(L"MyQueryKeyValue\n");
	TRACE(pszKey);
	TRACE(L"\n");
	TRACE(pszValue);
	TRACE(L"\n");
	if (regKey.Open(hKey,pszKey,KEY_QUERY_VALUE)==ERROR_SUCCESS)
	{
		if(regKey.QueryValue(buffer,pszValue,&strlen)==ERROR_SUCCESS)
			str = buffer;
	}

	return str;
}

//////////////////////////////////////////////////////////////////////////

//查询COM口

/*BOOL CControlData::QueryCOM()
{

	CString strTemp;

	int i;
	for (i=0; i<=20; i++)
	{
		CString strCom;
		strCom = (LPCTSTR)MyQueryKeyValue(HKEY_LOCAL_MACHINE,L"HARDWARE\\DEVICEMAP\\SERIALCOMM",\
			GetEnumIndex(i,ID_ENUMUSBCOM));
		if(!strCom.IsEmpty())
		{
			m_CurrentCOM = strCom;
			strTemp = (LPCTSTR)GetUSBRegistrySubKey( HKEY_LOCAL_MACHINE, \
					L"SYSTEM\\CurrentControlSet\\Enum\\USB\\Vid_0416&Pid_6851" ); //USBCOM
			if(!strTemp.IsEmpty())
			{
				m_USBCOMType = USBCOM;
				break;
			}
		}

		strCom = (LPCTSTR)MyQueryKeyValue(HKEY_LOCAL_MACHINE,L"HARDWARE\\DEVICEMAP\\SERIALCOMM",\
			GetEnumIndex(i,ID_ENUMSERIALCOM));
		if(!strCom.IsEmpty())
		{
			m_CurrentCOM.Empty();
			m_CurrentCOM = strCom;

			strTemp = GetUSBRegistrySubKey( HKEY_LOCAL_MACHINE, \
					L"SYSTEM\\CurrentControlSet\\Enum\\USB\\Vid_067b&Pid_2303" ); //SERIALCOM
			if(!strTemp.IsEmpty())
			{
				m_USBCOMType = SERIALCOM;
				break;
			}
		}
	}

	if (strTemp.IsEmpty())
	{
		m_USBCOMType = UNKNOWNCOM;
		return FALSE;
	}
	
	return TRUE;
}*/

//////////////////////////////////////////////////////////////////////////

//查询调制解调器
//find you have already installed a modem or not
BOOL CControlData::QueryModem()
{
	CString strTemp;
	
	int i;
	for (i=0; i<=20; i++)
	{
		{
			strTemp = GetModemRegistrySubKey( HKEY_LOCAL_MACHINE, \
				L"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E96D-E325-11CE-BFC1-08002BE10318}" );
			
			if(!strTemp.IsEmpty())
				break;
		}
	}
	
	if (strTemp.IsEmpty())
		return FALSE;
	
	m_CurrentModem = strTemp;
	
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////

//建立拨号连接

void CControlData::SetupDial()
{
	TCHAR * lpszDeviceType = L"modem";	//RASDT_Modem;
	TCHAR * lpszPhoneBook = NULL;
	TCHAR * lpszEntry = L"";				//"AMOI-GPRS";
	TCHAR * lpszPhoneNumber = L"*99***1#";		//"*99***1#";
	TCHAR szEntry[256];
//	TCHAR szPhoneNumber[256];
//	TCHAR lpszDeviceName[256];
	

	if(m_CurrentModem.IsEmpty())
	{
		//AfxMessageBox("未安装调制解调器！");
		return;
	}
	
	lstrcpy(szEntry,(LPCTSTR)m_CurrentDial);
	lpszEntry = szEntry;

	//lstrcpy(szPhoneNumber,(LPCTSTR)m_DialNo);
	//lpszPhoneNumber = szPhoneNumber;
	
	if (1)//RasValidateEntryName(lpszPhoneBook, lpszEntry) != ERROR_ALREADY_EXISTS)//检测拨号网络是否存在
	{
		RASENTRY rasEntry;
		ZeroMemory(&rasEntry, sizeof(rasEntry));
		rasEntry.dwSize = sizeof(rasEntry);
		wcscpy(rasEntry.szLocalPhoneNumber,lpszPhoneNumber);
		rasEntry.dwfNetProtocols = RASNP_NetBEUI;
		rasEntry.dwFramingProtocol = RASFP_Ras;//设置为TcpIP协议

		//rasEntry.dwfNetProtocols = RASNP_Ip;		//设置为TcpIP协议
		//rasEntry.dwFramingProtocol = RASFP_Ppp;		//设置为PPP	
		wcscpy(rasEntry.szDeviceType, lpszDeviceType);
		wcscpy(rasEntry.szDeviceName, m_CurrentModem);	//"Mobile 115200");

		DWORD dwRV = RasSetEntryProperties(lpszPhoneBook, lpszEntry, &rasEntry,sizeof(rasEntry), NULL, 0);

		if (dwRV != SUCCESS)
			AfxMessageBox(L"建立拨号连接失败！");
	}
}
#define MAX_KEY_LENGTH 255 

int NameToComId(char* pName)
{
#define D_COM_KEYWORD "(COM"
#define D_COM_KEYWORD_SIZE (sizeof(D_COM_KEYWORD)-1)

	int Len = strlen(pName);
	int i;

	for (i = Len-1; i >= 0 && pName[i] != '('; i--); // find '('

	if (strncmp(pName + i, D_COM_KEYWORD, D_COM_KEYWORD_SIZE) == 0)
		return atoi(pName + i + D_COM_KEYWORD_SIZE);
	return -1;
}


int CControlData::GetValidComPort_V2(COMTYPE comtype,int *pComIdBuf, size_t ComIdBufSize)
{
#define BUFSIZE 1024  

	HDEVINFO		hDevInfo;
	SP_DEVINFO_DATA DeviceInfoData;  
	DWORD	i;
	size_t nRet = 0;
	int ComId = 0;

	if (pComIdBuf == NULL || ComIdBufSize == 0)
		return 0;

	if (comtype == COM_TYPE_COM)
	{
		//CString ComStr = L"USB\\Vid_1614&Pid_0407&Rev_0000&MI_02";  //8709
		CString ComStr = L"USB\\Vid_1614&Pid_0408&Rev_0000&MI_02";
		// Enumerate through all devices in Set.
		DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		hDevInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVCLASS_PORTS, 0, 0,DIGCF_PRESENT);
		if (hDevInfo == INVALID_HANDLE_VALUE)
			return 0;
		
		for (i = 0; SetupDiEnumDeviceInfo(hDevInfo, i,
				&DeviceInfoData); i++)
		{
			DWORD DataT;
			char  buffer[BUFSIZE] = {0};
			DWORD buffersize = sizeof(buffer);
		
			if (!SetupDiGetDeviceRegistryProperty(
						hDevInfo,
						&DeviceInfoData,
						SPDRP_HARDWAREID,
						&DataT,
						(PBYTE)buffer,
						buffersize,
						&buffersize))
			{
				continue;
			}
			{
				CString strCom;
				
				strCom.Format(_T("%s"),buffer);
				strCom.MakeLower();
				ComStr.MakeLower();
				if (strCom != ComStr)
					continue;
			}
		
			if (!SetupDiGetDeviceRegistryProperty(
						hDevInfo,
						&DeviceInfoData,
						SPDRP_FRIENDLYNAME,
						&DataT,
						(PBYTE)buffer,
						buffersize,
						&buffersize))
					continue;
		
			{
				char   Com[70] = "";	 
				int j=0;   
				CString strCom;
		
				strCom.Format(_T("%s"),buffer);
				for (j=0;j<strCom.GetLength();j++)	 
						Com[j]=(unsigned	 char)strCom[j];
				ComId = NameToComId(Com);	
			}
			if (ComId == -1)
				continue;
		
			pComIdBuf[nRet] = ComId;
			nRet++;
			if (nRet >= ComIdBufSize)
				break;
		}
		
		// Cleanup	 
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else if(comtype == COM_TYPE_MODEM)
	{
		// Enumerate through all devices in Set.
		DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		hDevInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVCLASS_MODEM, 0, 0,DIGCF_PRESENT);
		if (hDevInfo == INVALID_HANDLE_VALUE)
			return 0;
		
		for (i = 0; SetupDiEnumDeviceInfo(hDevInfo, i,
				&DeviceInfoData); i++)
		{
			DWORD DataT;
			char  buffer[BUFSIZE] = {0};
			DWORD buffersize = sizeof(buffer);
			CString strCom;
				
			if (!SetupDiGetDeviceRegistryProperty(
						hDevInfo,
						&DeviceInfoData,
						SPDRP_FRIENDLYNAME,
						&DataT,
						(PBYTE)buffer,
						buffersize,
						&buffersize))
					continue;
			
			strCom.Format(_T("%s"),buffer);

			if (strCom.Find(AMOI_MOBILE_MODEM) != -1)
			{
				nRet = 1;
				break;
			}
			
		}
		
		// Cleanup	 
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}


	return (int)nRet;
}


//find the com we needed is connect to the pc or not
BOOL CControlData::QueryCOM()
{
	int ComIdBuf[70] = {0};
	size_t ComIdBufSize = 1;
	
	if ( GetValidComPort_V2(COM_TYPE_MODEM, ComIdBuf, ComIdBufSize) == 0)
	{
		
		m_USBCOMType = UNKNOWNCOM;
		return FALSE;	
	}

	
	m_USBCOMType = USBCOM;
/*
	CString strTemp;
	CString strCom;
	int i;
	CStringArray CurCom;
	int  nPortCounts = 0; 
	
	{
		HKEY hKey = HKEY_LOCAL_MACHINE; 
		HKEY hSubKey; 
		long lResult	 = 0; 
		
		lResult = RegOpenKeyEx(hKey, L"HARDWARE\\DEVICEMAP\\SERIALCOMM", NULL, KEY_ALL_ACCESS, &hSubKey);
		if (ERROR_SUCCESS == lResult) 
		{
			DWORD dwIndexEnum	= 0; 
			DWORD dwRegType 	= REG_SZ; 
			DWORD dwKeyValueLen = MAX_PATH; 
			DWORD dwKeyNameLen	= MAX_PATH; 
			char  szKeyValue[MAX_PATH] = ""; 
			char  szKeyName[MAX_PATH]  = "";
			CString TempString;
	
			CurCom.RemoveAll();
			
			while (ERROR_SUCCESS == lResult)
			{
				dwKeyNameLen  = MAX_PATH;  
				dwKeyValueLen = MAX_PATH;  
				lResult = RegEnumValue( hSubKey, 
										dwIndexEnum++, 
										(LPWSTR)szKeyName, 
										&dwKeyNameLen, 
										NULL, 
										&dwRegType, 
										(unsigned char*)szKeyValue, 
										&dwKeyValueLen
									  );
	
				if(ERROR_SUCCESS == lResult) 
				{
					if ((dwKeyValueLen > 0) && (dwKeyNameLen > 0)) 
					{
						TempString.Format(_T("%s"),szKeyValue);
						CurCom.Add(TempString);
					}
				}
			}
			RegCloseKey(hSubKey); 
		} 
	}
	
	nPortCounts = CurCom.GetSize();
	for (i=0; i<nPortCounts; i++)
	{
		//query if there is a correct com be connected.
		strCom = CurCom[i];
		
		if(!strCom.IsEmpty())
		{
			TRACE(strCom);
            m_TempCOM = strCom;

			strTemp = GetUSBRegistrySubKey( HKEY_LOCAL_MACHINE, \
					"SYSTEM\\CurrentControlSet\\Enum\\USB\\Vid_1614&Pid_0407&MI_00" ); //USBCOM 8709
            if(!strTemp.IsEmpty())
			{
				//port name
				TRACE(strTemp);
				if(strTemp == strCom)
				{
                    m_LastCom = strCom;
                    WriteRegister(AMOI_NET_SETTING, _T("preCOM"), m_LastCom);
					m_USBCOMType = USBCOM;
					break;
				}
			}
		}
	}

	if (strTemp.IsEmpty())
	{
		m_USBCOMType = UNKNOWNCOM;
		return FALSE;
	}
	
	m_CurrentCOM = m_LastCom;

	TRACE(L"\n");
*/
	return TRUE;
}

int CControlData::GetDataCom()
{
#if 1
	int i,ComIdBuf[70] = {0};
	size_t ComIdBufSize = 1;
	
	GetValidComPort_V2(COM_TYPE_COM, ComIdBuf, ComIdBufSize);
	for (i=0;i<70;i++)
	{
		if (ComIdBuf[i] != 0)
			return ComIdBuf[i];
	}
#else
	CString strCom;
	int i;
	CStringArray CurCom;
	int  nPortCounts = 0; 

	{
		HKEY hKey = HKEY_LOCAL_MACHINE; 
		HKEY hSubKey; 
		long lResult     = 0; 
		
		lResult = RegOpenKeyEx(hKey, L"HARDWARE\\DEVICEMAP\\SERIALCOMM", NULL, KEY_ALL_ACCESS, &hSubKey);
		if (ERROR_SUCCESS == lResult) 
		{
		    DWORD dwIndexEnum   = 0; 
		    DWORD dwRegType     = REG_SZ; 
		    DWORD dwKeyValueLen = MAX_PATH; 
		    DWORD dwKeyNameLen  = MAX_PATH; 
		    char  szKeyValue[MAX_PATH] = ""; 
		    char  szKeyName[MAX_PATH]  = "";
			CString TempString;

			CurCom.RemoveAll();
			
		    while (ERROR_SUCCESS == lResult)
		    {
		        dwKeyNameLen  = MAX_PATH;  
		        dwKeyValueLen = MAX_PATH;  
		        lResult = RegEnumValue( hSubKey, 
		                                dwIndexEnum++, 
		                                (LPWSTR)szKeyName, 
		                                &dwKeyNameLen, 
		                                NULL, 
		                                &dwRegType, 
		                                (unsigned char*)szKeyValue, 
		                                &dwKeyValueLen
		                              );

				if(ERROR_SUCCESS == lResult) 
				{
					if ((dwKeyValueLen > 0) && (dwKeyNameLen > 0)) 
					{
						TempString.Format(_T("%s"),szKeyValue);
						CurCom.Add(TempString);
					}
				}
		    }
		    RegCloseKey(hSubKey); 
		} 
	}

	
	nPortCounts = CurCom.GetSize();
	for (i=0; i<nPortCounts; i++)
	{
		//query if there is a correct com be connected.
		strCom = CurCom[i];
		if(!strCom.IsEmpty())
		{
			TRACE(strCom);
			{
				CString strResult;
			
				CRegistryEx RegistryEx;

				//LPCTSTR strSection = L"SYSTEM\\CurrentControlSet\\Enum\\USB\\Vid_05c6&Pid_3197&MI_02"; //8703
				LPCTSTR strSection = L"SYSTEM\\CurrentControlSet\\Enum\\USB\\Vid_1614&Pid_0407&MI_02"; //8709
				if ( RegistryEx.Open( HKEY_LOCAL_MACHINE, L"" ) )
				{
					CStringArray aKeys;
					if ( RegistryEx.ListKey( strSection, aKeys ) )
					{
						int nCount;
						const int nSize = aKeys.GetSize();
						for ( nCount = 0; nCount < nSize && strResult.IsEmpty(); nCount++ )
						{
			
							CString strKey = CString( strSection ) + L"\\" + aKeys[ nCount ] + L"\\" + L"Device Parameters";
			
							CString strKey2 = RegistryEx.FindKey(strKey, L"PortName", strCom);
			
							if ( !strKey2.IsEmpty() )
							{
								char   ComId[70] = "";	 
								int	j=0;   
								for	(j=0;j<strCom.GetLength();j++)   
										ComId[j]=(unsigned	 char)strCom[j];
								int nPort = atoi((char *)(ComId + 3)); 	
								return nPort;
							}
						}
					}
					RegistryEx.Close();
				}
			}
		}
	}
#endif
	return 0;
}

//remark: you must first get the m_current com 
//then set apn
void CControlData::SetAPN()			//给当前的COM设置APN
{
	
	CRegKey regKey;
	CString szFile,szPath;
	TCHAR Return[255] = {0};

	//get the value of m_strInitKey for the value("SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E96D-E325-11CE-BFC1-08002BE10318}\.....)
	//the value of strTemp is the modem name(FriendlyName)
	//get the value of m_strInitKey which is the right path of key name "userinit" according to current com
	CString strTemp = GetModemRegistrySubKey( HKEY_LOCAL_MACHINE, \
		L"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E96D-E325-11CE-BFC1-08002BE10318}" );
	
	
	CString strIPV4= "AT+CGDCONT=1,\"IP\",\"";
	CString strAPN ;

{
	
	//写ini
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");

}
	GetPrivateProfileString(L"DialupSet",L"APN",L"1",Return,255,szFile);
	strAPN.Format(L"%s",Return);
	strIPV4 += strAPN;
	strIPV4 += "\"";
	TRACE(strIPV4);
	TRACE(L"\n");
	//if (regKey.Create(HKEY_LOCAL_MACHINE, m_strInitKey)==ERROR_SUCCESS) // should be m_currentmodem : zxj
		//regKey.SetValue(strIPV4,L"UserInit");

	SetAPN8709(strIPV4);
}

void CControlData::SetAPN8709(CString strIPV4)
{
	CRegKey regKey;
	CString strResult;
	CString strKey;
	CString strSection = L"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E96D-E325-11CE-BFC1-08002BE10318}";
	
	CString strAPN ;

	CRegistryEx RegistryEx;
	if ( RegistryEx.Open( HKEY_LOCAL_MACHINE, L"" ) )
	{
		CStringArray aKeys;
		if ( RegistryEx.ListKey( strSection, aKeys ) )
		{
			int nCount;
			const int nSize = aKeys.GetSize();
			for ( nCount = 0; nCount < nSize; nCount++ )
			{
				strKey = CString( strSection ) + L"\\" + aKeys[ nCount ] ;
				strResult = MyQueryKeyValue(HKEY_LOCAL_MACHINE,strKey,L"Model");

				if (strResult.Find(AMOI_MOBILE_MODEM) != -1)
				{
					m_strInitKey = strKey;
					TRACE(strKey);
					
					if (regKey.Create(HKEY_LOCAL_MACHINE, strKey)==ERROR_SUCCESS) // should be m_currentmodem : zxj
						regKey.SetValue(strIPV4,L"UserInit");
				}
			}
		}
		RegistryEx.Close();
	}
}
//////////////////////////////////////////////////////////////////////////

 //  设置初始化参数
BOOL CControlData::SetModemInit(CString strDevice)
{
	int ret = ERROR_SUCCESS;
	HDEVINFO hdi = NULL;
	SP_DEVINFO_DATA hDev = { 0 } ;
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();

	//char szClassName[100];
	LPGUID lpGUID= (LPGUID)&GUID_MODEM;
    //ReadRegister(AMOI_NET_SETTING, _T("preCOM"), m_LastCom);
	
	gModemConnect =  QueryCOM();

	if (!gModemConnect)
	{
		//CString strvalue=pApp->g_GetText(IDS_MODEM_DISCONNECTED,pApp->m_nLandId);
		//AfxMessageBox(strvalue);
		
		//strTemp save the modem name;
		//this function assign the value of m_strInitKey
	}
    else
    {
    	SetAPN();
    }

	return TRUE;
}
 

