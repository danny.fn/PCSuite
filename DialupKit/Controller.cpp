// Controller.cpp: implementation of the CController class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Controller.h"
#include "DialupKit.h"
#include "DialupKitDlg.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define ID_TIMER_SETUPPROCESS      0
//#define CONTROLLER_STR_MODEM_DISCONNECTED   "Please ensure Amoi Mobile is connected."
extern bool   gModemConnect;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SINGLETON(CController);

// 检测Modem是否连接，主要是为产生一个MessageBox
BOOL CController::DetectModemStatus(CString &strDevice)
{

    if (!CControlData::Instance()->DetectModemConnect(strDevice))
    {
		// MODIFIED BY SHAOZW , LOAD STRING FROM RESOURCE
		/*CString str;

		TRACE(strDevice);
		CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
		str=pApp->g_GetText(IDS_MODEM_DISCONNECTED,pApp->m_nLandId);
		
		m_pGPRSDlg->MessageBox(str, NULL, MB_ICONINFORMATION);*/
        //m_pGPRSDlg->MessageBox(CONTROLLER_STR_MODEM_DISCONNECTED);

        return FALSE;
    }
    
    return TRUE;
}

// 取得主对话框指针并初始化RAS信息
void CController::Initialize(CDialupKitDlg *pGPRSDlg) 
{
    if (NULL == pGPRSDlg) 
        return;

    m_pGPRSDlg = pGPRSDlg; 

    // 从注册表读取C网、G网参数，如果没有，就用默认参数初始化，否则用注册表中的参数
    CControlData::Instance()->InitSystem(pGPRSDlg->m_hWnd);

    // 在"网络邻居中"创建图标前，先检测是否有夏新手机的Modem
    CString strDevice;
    if (!DetectModemStatus(strDevice))
        return;


//设置modem的初始化命令
    CControlData::Instance()->SetModemInit(strDevice); 


    // 在"网络邻居中"创建图标(设备名由DetectModemStatus得到)
    CControlData::Instance()->EsTablishLinkAttribute(strDevice);
}

// 设置C网、G网参数(这个函数最后需要完成C网、G网参数设置)
BOOL CController::SetGPRS()
{
    if (!CControlData::Instance()->SetGPRS())
        return FALSE;

    return TRUE;
}

// 发起拨号
BOOL CController::BeginConnect()
{
    CString strDevice;
    if (!DetectModemStatus(strDevice))
        return TRUE;

	//add by lishch 
	//when connect set the apn to the current com
	//m_current com was set by function Is8512modemConnect
	CControlData::Instance()->SetAPN();

	// ADD BY SHAOZW
	// here we need to virify that the phone-book entry is exist,
	// and if the entry is not exist, create it
	BOOL bEntryExist = FALSE;
	bEntryExist = IsPhoonBookEntryExist();
//    if(!bEntryExist)  // create the entry
//	{
		CControlData::Instance()->EsTablishLinkAttribute(strDevice);
//	}

    TRACE(L"1\n");
    if (!CControlData::Instance()->BeginConnect())
        return FALSE;

    TRACE(L"2\n");
    return TRUE;
}

// 停止拨号
BOOL CController::StopConnect()
{
    if (!CControlData::Instance()->StopConnect())
        return FALSE;

    return TRUE;
}

// 拨号后的状态，这个是消息处理函数，对应系统消息:WM_RAS_GETTINGDIALSTATUS，strInfo引用带回状态
void CController::OnGetDialStatus(WPARAM wParam, LPARAM lParam)
{
    CString strInfo;
    CControlData::Instance()->GetDialStatus(wParam, lParam, strInfo);
}


void CController::SetRASConnHandle(HRASCONN hRasConn)  // ADD BY SHAOZW
{
	CControlData::Instance()->SetRASConnHandle(hRasConn);
}

HRASCONN CController::GetRASConnHandle() // ADD BY SHAOZW
{
	return CControlData::Instance()->GetRASConnHandle();
}


int CController::GetSelIndex()
{
    CListCtrl *pListCtrl = NULL;
    pListCtrl = (CListCtrl *)(m_pGPRSDlg->GetDlgItem(IDC_LIST_GPRS));
    if (NULL == pListCtrl)
        return -1;

    POSITION pos = pListCtrl->GetFirstSelectedItemPosition();
    if (pos == NULL)
        return -1;
    
    return pListCtrl->GetNextSelectedItem(pos);
}

BOOL CController::IsConnectionActive()
{
	return CControlData::Instance()->IsConnectionActive();
}
	

BOOL CController::IsPhoonBookEntryExist()   // ADD BY SHAOZW
{
    return CControlData::Instance()->IsPhoonBookEntryExist();
}
