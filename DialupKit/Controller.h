// Controller.h: interface for the CController class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTROLLER_H__D56323D3_3FB0_4DDA_AFB4_CEA11D03BD5C__INCLUDED_)
#define AFX_CONTROLLER_H__D56323D3_3FB0_4DDA_AFB4_CEA11D03BD5C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Singleton.h"
#include "ras.h"  // ADDED BY SHAOZW
#include "ControlData.h"

class CDialupKitDlg;
class CController
{
    DEFINE_SINGLETON(CController);
public:
    void Initialize(CDialupKitDlg *pGPRSDlg);    // 取得主对话框指针并初始化RAS信息
    BOOL SetGPRS();                         // 设置C网、G网参数(这个函数最后需要完成C网、G网参数设置)
    BOOL BeginConnect();                    // 发起拨号
    BOOL StopConnect();                     // 停止拨号
    
    BOOL DetectModemStatus(CString &strDevice);             // 检测Modem是否连接，主要是为产生一个MessageBox

    void OnGetDialStatus(WPARAM wParam, LPARAM lParam);     // 拨号后的状态，这个是消息处理函数，对应系统消息:WM_RAS_GETTINGDIALSTATUS


	int  GetSelIndex();
	
	void SetRASConnHandle(HRASCONN hRasConn);  // ADD BY SHAOZW
	HRASCONN GetRASConnHandle();

	BOOL IsConnectionActive();                  // ADD BY SHAOZW

	BOOL IsPhoonBookEntryExist();              // ADD BY SHAOZW

	
	// Operate 
protected:

// Attribute
private:
    CDialupKitDlg *m_pGPRSDlg;                   // 主对话框指针

};

#endif // !defined(AFX_CONTROLLER_H__D56323D3_3FB0_4DDA_AFB4_CEA11D03BD5C__INCLUDED_)
