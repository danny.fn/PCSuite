// DialAPNDB.cpp: implementation of the DialAPNDB class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "dialupkit.h"
#include "DialAPNDB.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define MAX_PATH		  260

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DialAPNDB::DialAPNDB(CString szDBName, CString szSql)
{
    m_szDBName = szDBName;
    m_pRecordSet = NULL;
    m_bOpenState = FALSE;
    //AfxOleInit(); 
    m_pConnection = ADO_Connect(szDBName);
    m_bOpenState = ADO_OpenDB(szSql);
}

DialAPNDB::~DialAPNDB()
{
     ADO_Close();
}

_ConnectionPtr DialAPNDB:: ADO_Connect(CString szDBName)
{
    HRESULT hr;
    CString sPath, openStr;
    _ConnectionPtr m_pConnection = NULL;
    GetModuleFileName(NULL,sPath.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
    sPath.ReleaseBuffer ();
    int nPos;
    nPos=sPath.ReverseFind (TCHAR('\\'));
    sPath=sPath.Left (nPos+1);
    sPath += szDBName;
    //  AfxMessageBox(sPath);
    openStr = _T("Provider=Microsoft.Jet.OLEDB.4.0;Data Source =") + sPath;
    try
    {
        hr = m_pConnection.CreateInstance(_T("ADODB.Connection"));///创建Connection对象
        if(SUCCEEDED(hr))
        {
            hr = m_pConnection->Open((_bstr_t)openStr/*"Provider=Microsoft.Jet.OLEDB.4.0;Data Source = .\\AmoiPCSync.mdb"*/,"","",adModeUnknown);///连接数据库
            ///上面一句中连接字串中的Provider是针对ACCESS2000环境的，对于ACCESS97,需要改为:Provider=Microsoft.Jet.OLEDB.3.51;  }
        }
    }   
    catch(_com_error e)///捕捉异常
    {
        CString errormessage;
        CString str = "DATABASE FAIL";
        errormessage.Format(_T("%s"),e.ErrorMessage());
        errormessage = str + errormessage;
        AfxMessageBox(errormessage);///显示错误信息
        return m_pConnection;
    } 
    return m_pConnection;
}

BOOL DialAPNDB::ADO_OpenDB(CString szSql)
{
    HRESULT hr = E_FAIL;
    
    try
    {       
        hr = m_pRecordSet.CreateInstance("ADODB.Recordset");///创建Connection对象
        if(SUCCEEDED(hr))
        {
            hr = m_pRecordSet->Open( (_variant_t)szSql,
                _variant_t((IDispatch*)m_pConnection,true),
                adOpenDynamic,
                adLockOptimistic,
                adCmdText);
            #ifdef _ERROR_DEBUG_           
            if(SUCCEEDED(hr))
            {
                AfxMessageBox(_T("Open db success!"));
            }
            else
            {
                CString str;
                str.Format(_T("open db open hr:%d"), hr);
                AfxMessageBox(str);
            }
            #endif
        }
        #ifdef _ERROR_DEBUG_ 
        else
        {
            CString str;
            str.Format(_T("open db create hr:%d"), hr);
            AfxMessageBox(str);
        }
        #endif
    }
    catch(_com_error e)///捕捉异常
    {
        CString errormessage = "Connect to database failed";//"连接数据库失败!\r\n错误信息";
        //  errormessage.Format(/*,e.ErrorMessage()*/);
        AfxMessageBox(errormessage);///显示错误信息
        m_bOpenState = FALSE;
    } 
    return SUCCEEDED(hr);
}

BOOL DialAPNDB::ADO_IsEof()
{
   return m_pRecordSet->EndOfFile;
}

_variant_t DialAPNDB::ADO_GetValue(char* pItem)
{
   _variant_t Value;
   Value = m_pRecordSet->GetCollect((_variant_t)pItem);
    
   return Value;
}

void DialAPNDB::ADO_Execute(CString szSql,CString szErr)
{
    HRESULT hr = E_FAIL;
    CComVariant    vRecords;
    try
    {
        hr = m_pConnection->Execute ( ( _bstr_t ) szSql, &vRecords, adCmdText );
    } 
    catch ( _com_error e )
    {
        CString msg;
        msg.Format(_T("%s"),e.ErrorMessage());
        msg = szErr + msg;
        AfxMessageBox(msg);///显示错误信息
    }
}

void DialAPNDB::ADO_MoveNext( )
{
    m_pRecordSet->MoveNext( );
}

void DialAPNDB::ADO_Close()
{
   m_pRecordSet->Close();
}
