#if !defined(AFX_DIALSETDLG_H__E8388365_E464_4DDF_AD53_C3C2EF01C47F__INCLUDED_)
#define AFX_DIALSETDLG_H__E8388365_E464_4DDF_AD53_C3C2EF01C47F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialSetDlg.h : header file
//
#include "DialAPNDB.h"
#include "Resource.h"
#include "RgnDialog.h"
#include "StaticTrans.h"
#include "afxwin.h"
//#include "afxtempl.h"
//#include <afxdb.h>
//#include <odbcinst.h>
//#include "atlbase.h"

/////////////////////////////////////////////////////////////////////////////
// CDialSetDlg dialog


typedef CArray<CStringArray*,CStringArray*> CStrArrS;
class CDialSetDlg : public CRgnDialog
{
// Construction
public:
	DialAPNDB *m_pAPNDB;
	CDialSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialSetDlg)
	enum { IDD = IDD_DLG_NETSET };
	CComboBox	m_hDialNo;
	CComboBox	m_hCountryCombo;
	CString	m_szAPN;
	CString	m_szIP;
	CString	m_szPass;
	CString	m_szUser;
	CString	m_szCountry;
	CString	m_szConnectName;
	CString	m_szNO;
	CString m_szHomepage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
private:
	CBitmap  m_Bitmap;

// Implementation
protected:
	void TransDlg();
	// Generated message map functions
	//{{AFX_MSG(CDialSetDlg)
	virtual void OnOK();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC );
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCountrycombo();
	//afx_msg void OnSelchangeNetworkcombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CStaticTrans m_Country;
	CStaticTrans m_APNName;
	CStaticTrans m_ConName;
	CStaticTrans m_UserName;
	CStaticTrans m_pas;
	CStaticTrans m_Title;
	CEdit m_APNEDIT;
	CEdit m_UerNameEdit;
	CButton m_ok;
	CButton m_bcancel;
	
public:
	afx_msg void OnBnClickedOk();

};

//_ConnectionPtr ADO_Connect(CString szDBName);
//BOOL ADO_OpenDB(_ConnectionPtr m_pConnection,CString szSql);
//BOOL ADO_IsEof();
//_variant_t ADO_GetValue(char* pItem);
//void ADO_Execute(_ConnectionPtr m_pConnection,CString szSql,CString szErr);
//void ADO_MoveNext( );
//void ADO_Close();


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALSETDLG_H__E8388365_E464_4DDF_AD53_C3C2EF01C47F__INCLUDED_)
