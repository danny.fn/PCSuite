// DialupToolDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DialupKit.h"
#include "DialupKitDlg.h"
#include ".\dialupkitdlg.h"
//#include ".\dialuptooldlg.h"
#include "common.h"
#include "Controller.h"
#include "DialSetDlg.h"
#include "ConnectionDlg.h"
#include "CUsageDlg.h"
#include "ConnectionHistory.h"
#include "shlwapi.h"
#include "HelpDlg.h"
#include "shellapi.h"
#include "StatusDlg.h"

#define MAX_PATH		  260

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
CFont Minfont;
CFont Bigfont;
CFont Newfont;
CFont Onefont;
int   Receive = 0;
int	  Transmit = 0;
int 	PhoneStatus = 1; //表示手机网络状态. 1:no signal;2:GSM;3:3G,4:H3G
BOOL	m_IsAddFone;//判断是否要添加字体
RECT	DlgOnScr;
BOOL	IsResizeUsage; //用来判断主窗口回复大小时是否要同时回复usage
TDialStatus	m_Dialstatus = STATUS_IDLE;
CString	MSISDN;

int CALLBACK EnumFontFamProc(LPENUMLOGFONT lpelf,LPNEWTEXTMETRIC lpntm,DWORD nFontType,long lparam);

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CRgnDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL OnInitDialog() ;
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

CAboutDlg::CAboutDlg() : CRgnDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CRgnDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	// TODO: Add extra initialization here
	GetDlgItem(IDOK)->SetWindowText(pApp->g_GetText(T_OK,pApp->m_nLandId));
	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

BEGIN_MESSAGE_MAP(CAboutDlg, CRgnDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialupKitDlg dialog

CDialupKitDlg::CDialupKitDlg(CWnd* pParent /*=NULL*/)
	: CRgnDialog(CDialupKitDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialupKitDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDialupKitDlg::DoDataExchange(CDataExchange* pDX)
{
	CRgnDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialupKitDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_BUTTON_CONTROL, m_bControl);
	DDX_Control(pDX, IDC_STATE, m_TState);
	DDX_Control(pDX, IDC_SETTING, m_bSetting);
	DDX_Control(pDX, IDC_HELP, m_bHelp);
	DDX_Control(pDX, IDC_CONNSTATU1, m_HSTATUS1);
	DDX_Control(pDX, IDC_CONNSTATU2, m_HSTATUS2);
	DDX_Control(pDX, IDC_CONNSTATU3, m_HSTATUS3);
	DDX_Control(pDX, IDC_SIGNAL1, m_Signal1);
	DDX_Control(pDX, IDC_IDC_SIGNAL2, m_Signal2);
	DDX_Control(pDX, IDC_BITMAP_SIGNAL, m_bSingnalbitmap);
	DDX_Control(pDX, IDC_SPEED, m_Speed);
	DDX_Control(pDX, IDC_BUTTON_USAGE, m_bUsage);
	DDX_Control(pDX, IDC_BUTTON_INQ, m_bInq);
}

BEGIN_MESSAGE_MAP(CDialupKitDlg, CRgnDialog)
	//{{AFX_MSG_MAP(CDialupKitDlg)
	ON_WM_ERASEBKGND()
	ON_WM_DEVICECHANGE()
	ON_WM_DESTROY()
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_WM_FONTCHANGE()
	ON_MESSAGE(WM_RAS_GETTINGDIALSTATUS, OnGetDialStatus)
	ON_COMMAND(ID_SETTINGS_CONNECTION, OnMenuConnectionSetting)
	ON_COMMAND(ID_SETTINGS_PROFILE, OnMenuProfile)
	ON_COMMAND(ID_HELP_HELP, OnMenuHelp)
	ON_COMMAND(ID_HELP_ABOUT, OnMenuAbout)
	ON_COMMAND_RANGE(ID_LANG_START, ID_LANG_START+20, OnMenulanguage)
	ON_COMMAND(ID_MENU_EXIT, OnMenuExit)
	ON_COMMAND(ID_MENU_OPEN, OnMenuOpen)
	ON_MESSAGE(WM_TRAY_NOTIFY, OnTrayNotify)
	ON_MESSAGE(WM_COMMNOTIFY, HandleCommMessage)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_CONTROL, OnBnClickedButtonControl)
	ON_BN_CLICKED(IDC_SETTING, OnBnClickedSetting)
	ON_BN_CLICKED(IDC_HELP, OnBnClickedHelp)
	ON_BN_CLICKED(IDC_BUTTON_USAGE, OnBnClickedButton1)
	ON_WM_WINDOWPOSCHANGING()
	ON_BN_CLICKED(IDC_BUTTON_INQ, &CDialupKitDlg::OnBnClickedButtonInq)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialupKitDlg message handlers

BOOL CDialupKitDlg::OnInitDialog()
{
	CFont stFont;
	int Open;
	UINT ICONID;
/*	CFont	font;	
	  font.CreateStockObject(SYSTTEM_FONT);   
	  LOGFONT lf;	
	  memset(&lf,	0,	 sizeof(LOGFONT));	 
	  font.GetLogFont(&lf); 
	  EnumFontFamilies*/ 
	  m_IsAddFone = TRUE;
{
	LOGFONT lf;
	lf.lfCharSet = DEFAULT_CHARSET; // Initialize the LOGFONT structure
	memset(lf.lfFaceName, 0, sizeof(lf.lfFaceName));
	CClientDC dc(this);
	// Enumerate the font families
	::EnumFontFamiliesEx((HDC) dc,&lf, (FONTENUMPROC) EnumFontFamProc,(LPARAM) this,0);
}
	
	// 取得主对话框指针并初始化RAS信息
    CController::Instance()->Initialize(this);

	//m_Bitmap.LoadBitmap(IDB_BITMAP2);
	//SetBackgroundBitmap(&m_Bitmap,RGB(255,0,255));
	
	SetWindowText(L"INQ1 Mobile Modem.");
	CRgnDialog::OnInitDialog();

	OnSetFont(&stFont);

	//获取信息当前使用的是大字体还是小字体
	CWnd *pWnd = AfxGetMainWnd();			 // Get main window
	CDC *pDC = pWnd->GetDC();			   // Get device context
	int   nLogPixelsx = pDC->GetDeviceCaps(LOGPIXELSX);   
	Isbigtypeface = FALSE;
	if (nLogPixelsx >= 120)
		Isbigtypeface = TRUE;
	
	pWnd->ReleaseDC(pDC);

	//添加或设置字体
	if (m_IsAddFone)
		AddFone();
	else
	{
		LOGFONT stBigFont;
		memset(&stBigFont,0,sizeof(LOGFONT));
		if (Isbigtypeface)
			stBigFont.lfHeight=20;//设置字体高度为12
		else
			stBigFont.lfHeight=16;//设置字体高度为12
		stBigFont.lfWeight=FW_DONTCARE;
		stBigFont.lfCharSet=DEFAULT_CHARSET;
		wcscpy(stBigFont.lfFaceName,L"Modena Condensed Regular");//设置字样为黑体
		Newfont.CreateFontIndirect(&stBigFont);
		m_bSetting.SetFont(&Newfont);
		m_bHelp.SetFont(&Newfont);
		m_Signal2.SetFont(&Newfont);
		m_Speed.SetFont(&Newfont);
		m_bUsage.SetFont(&Newfont);
		m_bInq.SetFont(&Newfont);
	}
	
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		pSysMenu->AppendMenu(MF_SEPARATOR);
		pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, GetText(T_ABOUTMENU));
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	CreateNewFone();

	m_TState.SetFont(&Minfont);
	m_HSTATUS2.SetFont(&Minfont);

	m_Dialstatus = STATUS_IDLE;

	if (gModemConnect == FALSE)
	{
		m_bControl.SetBitmaps(IDB_BITMAP_NOUSB,RGB(255,0,255),NULL,0);
		m_TState.SetWindowText(GetText(T_USERDO1));
		m_HSTATUS2.SetWindowText(GetText(T_NOCONNECT));
		m_HSTATUS3.SetWindowText(GetText(T_DISCONNECT));
		ICONID = IDI_NOCON;
	}
	else
	{
		if (!CController::Instance()->IsConnectionActive())
		{
			m_Dialstatus = STATUS_CONNECT;
 			m_bControl.SetBitmaps(IDB_BITMAP_CONNECT,0xFFFFFF00,NULL,0);
			m_TState.SetWindowText(GetText(T_USERDO2));
			m_HSTATUS2.SetWindowText(GetText(T_CONNECTUSB));
			m_HSTATUS3.SetWindowText(GetText(T_DEVICEDET));
			ICONID = IDI_NOCON;
		}
		else
		{
			SetTimer(TIMER_DETECT_CONNECTION_STATUS, DETECT_CONNECTION_ELAPSE, NULL);
 			m_bControl.SetBitmaps(IDB_BITMAP_DISCONNECT,0xFFFFFF00,NULL,0);
			m_TState.SetWindowText(GetText(T_USERDO4));
			m_HSTATUS2.SetWindowText(GetText(T_CONNECTUSB));
			m_HSTATUS3.SetWindowText(GetText(T_DEVICECONNECTED));
			ICONID = IDI_NEDIUM;
		}
	}
	m_bControl.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	m_bControl.DrawTransparent(TRUE);
	m_bControl.DrawBorder(FALSE,FALSE);
	m_bControl.SetPressedStyle(CExtendButton::BTNST_PRESSED_TOPBOTTOM,FALSE);
	
	m_bUsage.SetBitmaps(IDB_BUTTONBK,RGB(255,0,255),IDB_BUTTONBK,0);
	m_bUsage.SetWindowText(GetText(T_USAGE));
	m_bUsage.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	m_bUsage.DrawBorder(FALSE,FALSE);
	m_bUsage.DrawTransparent(TRUE);

	m_bInq.SetBitmaps(IDB_BUTTONBK,RGB(255,0,255),IDB_BUTTONBK,0);
	//m_bInq.SetWindowText(GetText(T_USAGE));
	m_bInq.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	m_bInq.DrawBorder(FALSE,FALSE);
	m_bInq.DrawTransparent(TRUE);	

	//m_bSetting.SetBitmaps(IDB_MENUBK,RGB(255,0,255),IDB_MENUBK,0);
	m_bSetting.SetWindowText(GetText(T_SETTING));
	m_bSetting.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	//m_bSetting.SetColor(1, RGB(255,255,255), TRUE);
	//m_bSetting.SetColor(3, RGB(255,255,255), TRUE);
	//m_bSetting.SetColor(5, RGB(255,255,255), TRUE);
	m_bSetting.DrawBorder(FALSE,FALSE);
	m_bSetting.DrawTransparent(TRUE);

	//m_bHelp.SetBitmaps(IDB_MENUBK,RGB(255,0,255),IDB_MENUBK,0);	
	m_bHelp.SetWindowText(GetText(T_HELP));
	m_bHelp.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	m_bHelp.DrawBorder(FALSE,FALSE);
	m_bHelp.DrawTransparent(TRUE);
	//m_bHelp.SetColor(1, RGB(255,255,255), TRUE);
	//m_bHelp.SetColor(3, RGB(255,255,255), TRUE);
	//m_bHelp.SetColor(5, RGB(255,255,255), TRUE);
	
	m_TState.SetBackColor(TRANS_BACK);
	m_TState.SetTextColor(RGB(255,255,255));
	m_HSTATUS1.SetWindowText(GetText(T_HANDSSTATUS));
	m_HSTATUS1.SetFont(&Bigfont);
	m_HSTATUS1.SetTextColor(RGB(255,255,255));
	m_HSTATUS2.SetTextColor(RGB(255,255,255));
	m_HSTATUS3.SetTextColor(RGB(255,255,255));
	m_HSTATUS3.SetFont(&Onefont);

	m_Signal1.SetWindowText(GetText(T_SINGE));
	m_Signal1.SetFont(&Bigfont);
	m_Signal1.SetTextColor(RGB(255,255,255));
	m_Signal2.SetWindowText(GetText(T_NOSINGE));
	m_Signal2.SetTextColor(RGB(255,255,255));

	m_Speed.SetBackColor(TRANS_BACK);
	m_Speed.SetTextColor(RGB(255,255,255));
	m_Speed.SetWindowText(L"0kb/0kb");
	
	MainDlgStatue = 1;

	m_pRS232 = new CRS232();
	Open = OpenCom();
	if (Open == ERR_FAILED && gModemConnect == FALSE)	//打开端口失败，默认为用户从菜单启动，不判断当前情况显示主窗体
	{
		m_Show = TRUE;
		m_Check = FALSE;
		SetTrayIcon(1, ICONID);
		SetTimer(TIMEE_RELEASE_TMEP, 3000, NULL);
	}
	else
	{
		m_Show = FALSE;
		m_Check = TRUE;
	}
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

int CALLBACK EnumFontFamProc(LPENUMLOGFONT lpelf,
LPNEWTEXTMETRIC lpntm,DWORD nFontType,long lparam)

{
	if (wcscmp(lpelf->elfFullName,L"Modena Condensed Regular") == 0)
	{
		m_IsAddFone = FALSE;//判断是否要添加字体
		return 0; //返回0不再回来字串信息
	}
	return 1;
}


BOOL CDialupKitDlg::OnDeviceChange(UINT nEventType, DWORD dwData)
{
	CString strDevice;

	if (MainDlgStatue == 0)
		return TRUE;

	CloseCom();
	KillTimer(TIMER_OPEN_COM);
	SetTimer(TIMER_OPEN_COM, DETECT_CONNECTION_ELAPSE, NULL);

	//如果是在关机充电状态拔下usb，关闭
	if (m_Check)
	{
		PostMessage(WM_QUIT, NULL, NULL);
	}
	return TRUE;
}

void CDialupKitDlg::OnDestroy() 
{
	/*CMyDlg::OnDestroy();
	
	// TODO: Add your message handler code here
	int ret = 0;
	
	
	if(!m_bShutDown)  // just hide the window
	{
		ShowWindow(SW_HIDE);
		return;
	}*/

	// check whether there is a connection still active
	if(m_pRS232 != NULL)
	{
		if(m_pRS232->IsOpen() == TRUE)
		{
			m_pRS232->Close();
		}
	
		delete m_pRS232;
	
		m_pRS232 = NULL;
	}
	
	SetTrayIcon(3, TrayIcon); // delete the tray icon 
	
	KillTimer(TIMER_OPEN_COM);
	KillTimer(TIMEE_RELEASE_TMEP);
	KillTimer(TIMER_DETECT_CONNECTION_STATUS);
}

void CDialupKitDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CRgnDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDialupKitDlg::OnPaint() 
{
	
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CRgnDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDialupKitDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDialupKitDlg::OnBnClickedButtonControl()
{
	// TODO: 在此添加控件通知处理程序代码
	if (gModemConnect == FALSE)
	{	
		CString str;

		CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
		str=pApp->g_GetText(T_MODEM_DISCONNECTED,pApp->m_nLandId);
		
		MessageBox(str, NULL, MB_ICONINFORMATION);
	}
	else
	{
		if (!CController::Instance()->IsConnectionActive())
		{
			if ((ConnectionSet == 3 || ConnectionSet == 2) && (PhoneStatus != 3) && (PhoneStatus != 4))
			{
				MessageBox(GetText(T_CONNECTSET3), NULL, MB_ICONINFORMATION);
			}
			else
			{
				CString strAPN ;
				char		senapn[50] = {0};
				int iSize=50,pCount;

				{
					CString szFile,szPath;
					TCHAR Return[255] = {0};
	
					//写ini
					GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
					szPath.ReleaseBuffer ();
					int mPos;
					//AfxMessageBox(szPath);
					mPos=szPath.ReverseFind ('\\');
					szPath=szPath.Left (mPos);
					szFile = szPath + _T("\\ModemSetting.ini");
					GetPrivateProfileString(L"DialupSet",L"APN",L"1",Return,255,szFile);
					strAPN.Format(L"%s",Return);

				}
				
				sprintf(senapn,"APN:%S",strAPN);
				m_pRS232->Send((BYTE *)senapn, iSize, &pCount);


				if (m_Dialstatus != STATUS_BEGINCONNECT)
				{
					KillTimer(TIMER_CAN_DIALUP);
					m_Dialstatus = STATUS_BEGINCONNECT;
					//CController::Instance()->BeginConnect();
					CStaticSetText(IDC_STATE, GetText(T_OPENPORT));
					SetTimer(TIMER_DIALUP, 10000, NULL);
				}
			}
		}
		else
		{
			if ((m_Dialstatus != STATUS_DISCONNECTING) && (m_Dialstatus != STATUS_BEGINCONNECT))
 				Disconnect();
		}
	}
}

void CDialupKitDlg::SetTrayIcon(int dwMessage, UINT uID)
{
	NOTIFYICONDATA nid;

	if (TrayIcon == uID && (dwMessage == 1 || dwMessage == 2))
	{
		return;
	}
	
	nid.cbSize = (DWORD)sizeof(NOTIFYICONDATA); 
	nid.hWnd = this->m_hWnd;
	nid.uID = uID;
	nid.uFlags = NIF_ICON|NIF_MESSAGE|NIF_TIP ;
	nid.uCallbackMessage = WM_TRAY_NOTIFY;//自定义的消息名称
	
	nid.hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(uID));
	
	_tcscpy(nid.szTip, _T("INQ1 Mobile Modem"));	//信息提示条为“计划任务提醒”	

	if (dwMessage == 1)
	{
		Shell_NotifyIcon(NIM_ADD, &nid);//在托盘区添加图标
		TrayIcon = uID;
	}
	else if (dwMessage == 2)
	{
		Shell_NotifyIcon(NIM_MODIFY, &nid);//在托盘区修改图标
		TrayIcon = uID;
	}
	else	
	{
		Shell_NotifyIcon(NIM_DELETE, &nid);//在托盘区删除图标
		TrayIcon = 0;
	}
	
}

LRESULT CDialupKitDlg::OnTrayNotify(WPARAM wParam,LPARAM lParam)//wParam接收的是图标的ID，而lParam接收的是鼠标的行为
{
	if(wParam!=TrayIcon)
	{		
		return 0;
	}
	
	switch(lParam)
	{	
	case WM_RBUTTONUP://右键起来时弹出快捷菜单，这里只有一个“关闭”
		{	

			LPPOINT lpoint = new tagPOINT;
			CMenu Popmenu;
			CMenu pSubMenu;
			
			::GetCursorPos(lpoint);//得到鼠标位置
			
			/*pSubMenu.CreatePopupMenu();
			Insertmenu(&pSubMenu, ID_LANG_START, T_English);
			Insertmenu(&pSubMenu, (ID_LANG_START + 1), T_Chinese);*/
			
			Popmenu.CreatePopupMenu();
			//Popmenu.AppendMenu(MF_POPUP,(UINT)pSubMenu.m_hMenu,GetText(T_langauge)); 
			Insertmenu(&Popmenu, ID_MENU_OPEN, T_OPEN);
			Insertmenu(&Popmenu, ID_MENU_EXIT, T_EXIT);
			
			Popmenu.TrackPopupMenu(TPM_LEFTALIGN,lpoint->x,lpoint->y,this,NULL);
			delete lpoint;
		}
		break;
		
	case WM_LBUTTONDBLCLK://双击左键的处理
		{
			ShowWindow(SW_NORMAL);//简单的显示主窗口完事儿
			//SetForegroundWindow();
		}
		break;
	}	
	return 1;
	
}


BOOL CDialupKitDlg::Insertmenu(CMenu* pMenu, UINT nIDNewItem, UINT nStrId)
{
	CString str;
	
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	str=pApp->g_GetText(nStrId,pApp->m_nLandId);

 	pMenu->InsertMenu(0XFFFFFFFF, MF_BYPOSITION, nIDNewItem, str);

	return TRUE;
}

CString CDialupKitDlg::GetText(UINT nStrId)
{
	CString str;
	
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	str=pApp->g_GetText(nStrId,pApp->m_nLandId);

	return str;
}

void CDialupKitDlg::CStaticSetText(UINT NID, CString Str)
{
	int 	nTitleHeight = 0, nFrameWidth = 0;
	RECT	rDlgOnScr, rDlgClient, rCtrlOnScr, rCtrlClient;
	CWnd	*pWnd		 = GetDlgItem(NID);

	if (pWnd != NULL)
	{
		pWnd->SetWindowText(Str);
		
		pWnd->GetWindowRect(&rCtrlOnScr);
		
		GetWindowRect(&rDlgOnScr);
		
		(pWnd->GetParent())->GetClientRect(&rDlgClient);
		
		nTitleHeight = rDlgOnScr.bottom - rDlgOnScr.top  - rDlgClient.bottom;
		nFrameWidth  = (rDlgOnScr.right - rDlgOnScr.left - rDlgClient.right) / 2;
		
		rCtrlOnScr.right  = rCtrlOnScr.right - rDlgOnScr.left;
		rCtrlOnScr.bottom = rCtrlOnScr.bottom - rDlgOnScr.top;
		
		pWnd->GetClientRect(&rCtrlClient);
		rCtrlOnScr.left   = rCtrlOnScr.right - rCtrlClient.right - nFrameWidth;
		rCtrlOnScr.top	  = rCtrlOnScr.bottom - rCtrlClient.bottom - nTitleHeight;
		
		RedrawWindow(&rCtrlOnScr);
	}
	
}

void CDialupKitDlg::OnMenuConnectionSetting()
{
	CConnectionDlg dlg;
	dlg.DoModal();
}

void CDialupKitDlg::OnMenuProfile()
{
	CDialSetDlg dlg;
	dlg.DoModal();
}

void CDialupKitDlg::OnMenuHelp()
{
	CString    szFile,szFileModuleName;

	//HelpDlg dlg;
	//dlg.DoModal();
	GetModuleFileName(NULL,szFileModuleName.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
	szFileModuleName.ReleaseBuffer ();
    int mPos;
	mPos=szFileModuleName.ReverseFind ('\\');
	szFileModuleName=szFileModuleName.Left (mPos);	
	szFile = szFileModuleName + _T("\\mobile-modem-eng.chm");
	
	ShellExecute(NULL,L"open",szFile,NULL,NULL,SW_SHOWDEFAULT);
}

void CDialupKitDlg::OnMenuAbout()
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

void CDialupKitDlg::OnMenulanguage(UINT nID)
{
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	int i;

	i = nID - ID_LANG_START;

	switch (i)
	{
		case 0:
			pApp->m_nLandId = LANG_ENG;
			break;
		case 1:
			pApp->m_nLandId = LANG_CHT;
			break;
		case 2:
			pApp->m_nLandId = LANG_DAN;
			break;
		case 3:
			pApp->m_nLandId = LANG_FRE;
			break;
		case 4:
			pApp->m_nLandId = LANG_ITA;
			break;
		case 5:
			pApp->m_nLandId = LANG_DEU;
			break;
		case 6:
			pApp->m_nLandId = LANG_SWE;
			break;
		
		default:
			break;
	}
	pApp->m_pResConverion->SetLanguage((TLanguage)pApp->m_nLandId);

	
	if (gModemConnect == FALSE)
	{
		CStaticSetText(IDC_STATE,GetText(T_USERDO1));
		CStaticSetText(IDC_CONNSTATU2, GetText(T_NOCONNECT));
		CStaticSetText(IDC_CONNSTATU3, GetText(T_DISCONNECT));
	}
	else
	{
		if (!CController::Instance()->IsConnectionActive())
		{
			CStaticSetText(IDC_STATE,GetText(T_USERDO2));
			CStaticSetText(IDC_CONNSTATU2, GetText(T_CONNECTUSB));
			CStaticSetText(IDC_CONNSTATU3, GetText(T_DEVICEDET));
		}
		else
		{
			CStaticSetText(IDC_STATE,GetText(T_USERDO4));
			CStaticSetText(IDC_CONNSTATU2, GetText(T_CONNECTUSB));
			CStaticSetText(IDC_CONNSTATU3, GetText(T_DEVICECONNECTED));
		}
	}
	
	CStaticSetText(IDC_BUTTON1, GetText(T_USAGE));
	CStaticSetText(IDC_SETTING, GetText(T_SETTING));
	CStaticSetText(IDC_HELP, GetText(T_HELP));
	CStaticSetText(IDC_CONNSTATU1, GetText(T_HANDSSTATUS));
	CStaticSetText(IDC_SIGNAL1, GetText(T_SINGE));
	CStaticSetText(IDC_BUTTON_USAGE, GetText(T_USAGE));

	if (PhoneStatus == 1)
		CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_NOSINGE));
	else if (PhoneStatus == 2)
		CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_GSM));
	else if (PhoneStatus == 3 || PhoneStatus == 4)
		CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_3G));
	
}

void CDialupKitDlg::OnMenuExit()
{
	int a;
	
	//CDialog::OnCancel();
	if (CController::Instance()->IsConnectionActive())
	{
		a = MessageBox(GetText(T_EXITANDDISCONNECT), NULL, MB_YESNO);
		if (a == IDYES)
		{
			Disconnect();
			PostMessage(WM_QUIT, NULL, NULL);
			return;
		}
		else if (a == IDNO)
		{
			ShowWindow(SW_HIDE);
			return;
		}
	}

	PostMessage(WM_QUIT, NULL, NULL);
}

void CDialupKitDlg::OnMenuOpen()
{
	ShowWindow(SW_NORMAL);//简单的显示主窗口完事儿
}

void CDialupKitDlg::OnBnClickedSetting()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	CMenu Popmenu;
	CMenu pSubMenu;
	RECT  Rect;
	
	m_bSetting.GetWindowRect(&Rect);

	pSubMenu.CreatePopupMenu();
	Insertmenu(&pSubMenu, ID_LANG_START, T_English);
	Insertmenu(&pSubMenu, (ID_LANG_START + 1), T_Chinese);
	Insertmenu(&pSubMenu, (ID_LANG_START + 2), T_Denish);
	Insertmenu(&pSubMenu, (ID_LANG_START + 3), T_French);
	Insertmenu(&pSubMenu, (ID_LANG_START + 4), T_Italy);
	Insertmenu(&pSubMenu, (ID_LANG_START + 5), T_German);
	Insertmenu(&pSubMenu, (ID_LANG_START + 6), T_Sweden);

	switch (pApp->m_nLandId)
	{
		case LANG_ENG:
				//pSubMenu.SetMenuItemBitmaps(0,MF_BYPOSITION,&m_Bitmap,&m_Bitmap);	
				pSubMenu.CheckMenuItem(ID_LANG_START,MF_CHECKED);	  
			break;

		case LANG_CHT:
				//pSubMenu.SetMenuItemBitmaps(1,MF_BYPOSITION,&m_Bitmap,&m_Bitmap);	
				pSubMenu.CheckMenuItem((ID_LANG_START + 1),MF_CHECKED);	  
			break;

		case LANG_DAN:
				//pSubMenu.SetMenuItemBitmaps(2,MF_BYPOSITION,&m_Bitmap,&m_Bitmap);	
				pSubMenu.CheckMenuItem((ID_LANG_START + 2),MF_CHECKED);	  
			break;

		case LANG_FRE:
				//pSubMenu.SetMenuItemBitmaps(3,MF_BYPOSITION,&m_Bitmap,&m_Bitmap);	
				pSubMenu.CheckMenuItem((ID_LANG_START + 3),MF_CHECKED);	  
			break;

		case LANG_ITA:
				//pSubMenu.SetMenuItemBitmaps(4,MF_BYPOSITION,&m_Bitmap,&m_Bitmap);	
				pSubMenu.CheckMenuItem((ID_LANG_START + 4),MF_CHECKED);	  
			break;

		case LANG_DEU:
				//pSubMenu.SetMenuItemBitmaps(5,MF_BYPOSITION,&m_Bitmap,&m_Bitmap);	
				pSubMenu.CheckMenuItem((ID_LANG_START + 5),MF_CHECKED);	  
			break;

		case LANG_SWE:
				//pSubMenu.SetMenuItemBitmaps(6,MF_BYPOSITION,&m_Bitmap,&m_Bitmap);	
				pSubMenu.CheckMenuItem((ID_LANG_START + 6),MF_CHECKED);	  
			break;

		default: 
			break;
	}
		
	Popmenu.CreatePopupMenu();
	Insertmenu(&Popmenu, ID_SETTINGS_CONNECTION, T_CONNECTION);
	Insertmenu(&Popmenu, ID_SETTINGS_PROFILE, T_PROFILE);
	Popmenu.AppendMenu(MF_POPUP,(UINT)pSubMenu.m_hMenu,GetText(T_langauge)); 
	Popmenu.TrackPopupMenu(TPM_VERTICAL,Rect.left,Rect.bottom,this,NULL);
}

void CDialupKitDlg::OnBnClickedHelp()
{
	// TODO: 在此添加控件通知处理程序代码
	CMenu Popmenu;
	RECT  Rect;
	m_bHelp.GetWindowRect(&Rect);
	
	Popmenu.CreatePopupMenu();
	Insertmenu(&Popmenu, ID_HELP_HELP, T_HELP);
	Insertmenu(&Popmenu, ID_HELP_ABOUT, T_ABOUT1);
	Popmenu.TrackPopupMenu(TPM_VERTICAL,Rect.left,Rect.bottom,this,NULL);
}

/*
void CDialupKitDlg::OnBnClickedExit1()
{
	// TODO: 在此添加控件通知处理程序代码
	int a;
	
	if (CController::Instance()->IsConnectionActive())
	{
		a = MessageBox(GetText(T_EXITANDDISCONNECT), NULL, MB_YESNO);
		if (a == IDYES)
		{
			Disconnect();
			CDialog::OnCancel();
			return;
		}
		else if (a == IDNO)
		{
			ShowWindow(SW_HIDE);
			return;
		}
	}

	CDialog::OnCancel();
}
*/
void CDialupKitDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	GetWindowRect(&DlgOnScr);
	IsResizeUsage = FALSE;
	
	CCUsageDlg dlg;
	dlg.DoModal();
}

LRESULT CDialupKitDlg::OnGetDialStatus(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add with the status during dialing
	CString strInfo;
	
	CControlData::Instance()->GetDialStatus(wParam, lParam, strInfo);

	/*if (strInfo.IsEmpty() == 0)
	{
		return ERROR_SUCCESS;
	}*/
	
	CStaticSetText(IDC_STATE, strInfo);
	
	if ((RASCONNSTATE)wParam == RASCS_Connected)
	{
		m_Dialstatus = STATUS_CONNECT;
		m_bControl.SetBitmaps(IDB_BITMAP_DISCONNECT,RGB(255,0,255),NULL,0);
		//m_TState.SetWindowText(GetText(T_USERDO4));
		CStaticSetText(IDC_STATE, GetText(T_USERDO4));
		if (TrayIcon != IDI_NEDIUM)
		{
			SetTrayIcon(3, TrayIcon);
			SetTrayIcon(1, IDI_NEDIUM);
		}
	}
	else if((RASCONNSTATE)wParam == RASCS_Disconnected)
	{
		m_Dialstatus = STATUS_IDLE;
		if (gModemConnect == FALSE)
		{
			m_bControl.SetBitmaps(IDB_BITMAP_NOUSB,RGB(255,0,255),NULL,0);
			CStaticSetText(IDC_STATE, GetText(T_USERDO1));
		}
		else
		{
			m_bControl.SetBitmaps(IDB_BITMAP_CONNECT,RGB(255,0,255),NULL,0);
			CStaticSetText(IDC_STATE, GetText(T_USERDO2));
		}
		if (TrayIcon != IDI_NOCON)
		{
			SetTrayIcon(3, TrayIcon);
			SetTrayIcon(1, IDI_NOCON);
		}
	}
	
	return ERROR_SUCCESS;
}

int CDialupKitDlg::OpenCom()
{
	int i = 0;

	KillTimer(TIMER_OPEN_COM);
			
	if(m_pRS232->IsOpen() == TRUE)
	{
		return ERR_SUCCESS;
	}
	i = CControlData::Instance()->GetDataCom();

	if (i == 0)
		return ERR_FAILED;
	
	if(m_pRS232->Open(i, CBR_115200, NOPARITY, ONESTOPBIT, 8) != ERR_SUCCESS)
	{
		return ERR_FAILED;
	}
	
	{
		int iSize=12,pCount;
		CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
		//m_pRS232->Send((BYTE *)"MODEMBEGIN", iSize, &pCount);

		if (pApp->m_bGetlangandApn)
			m_pRS232->Send((BYTE *)"GETOPCO", iSize, &pCount);
		else
			m_pRS232->Send((BYTE *)"CANSTART", iSize, &pCount);
		
		SetTimer(TIMER_START_QUEST, 2000, NULL);
	}

	return ERR_SUCCESS;
}

int CDialupKitDlg::CloseCom()
{
	if(m_pRS232->IsOpen() == TRUE)
	{
		m_pRS232->Close();
	}

	return ERR_SUCCESS;
}

LRESULT CDialupKitDlg::HandleCommMessage(WPARAM wParam,LPARAM lParam)
{
	BYTE *pBuff = NULL;
	BYTE *pTemp = NULL;
	int iSize=100,pCount;
	int     n = 0,i;
	char pInform[100] = {0};

	pBuff = (UINT8 *)malloc(iSize);
	if(!pBuff)
	{
		free(pBuff);
		return 1;
	}
	memset(pBuff, 0x00,iSize);

	m_pRS232->Receive(pBuff, iSize, &pCount);
	
	//获取网络，信号
	//strcpy((char *)pBuff,"INFO:GSM111;MISDN");
	pTemp = pBuff;
	i = 0;
	for(n = 0; n <= strlen((char *)pBuff); n++)
	{
		i++;
		if (pBuff[n] == ';')
		{
			strncpy(pInform,(char*)pTemp,n);
			GetInformation(pInform);
			i = 0;
			pTemp = pBuff + (n + 1);
		}		
	}
	
	GetInformation((char *)pTemp);

	free(pBuff);

	return 1;
}

void CDialupKitDlg::CreateNewFone()
{
	LOGFONT stFont,stBigFont,stOneFont;
	memset(&stFont,0,sizeof(LOGFONT));
	if (Isbigtypeface)
		stFont.lfHeight=18;//设置字体高度为12
	else
		stFont.lfHeight=14;//设置字体高度为12
	stFont.lfWeight=FW_BOLD;
	stFont.lfCharSet=DEFAULT_CHARSET;
	wcscpy(stFont.lfFaceName,L"Modena Condensed Regular");//设置字样为黑体
	Minfont.CreateFontIndirect(&stFont);

	memset(&stBigFont,0,sizeof(LOGFONT));
	if (Isbigtypeface)
		stBigFont.lfHeight=26;//设置字体高度为12
	else
		stBigFont.lfHeight=22;//设置字体高度为12
	stBigFont.lfWeight=FW_BOLD;
	stBigFont.lfCharSet=DEFAULT_CHARSET;
	wcscpy(stBigFont.lfFaceName,L"Modena Condensed Regular");//设置字样为黑体
	Bigfont.CreateFontIndirect(&stBigFont);

	memset(&stOneFont,0,sizeof(LOGFONT));
	if (Isbigtypeface)
		stOneFont.lfHeight=18;//设置字体高度为12
	else
		stOneFont.lfHeight=14;//设置字体高度为12
	stOneFont.lfWeight=FW_DONTCARE;
	stOneFont.lfCharSet=DEFAULT_CHARSET;
	wcscpy(stOneFont.lfFaceName,L"Modena Condensed Regular");//设置字样为黑体
	Onefont.CreateFontIndirect(&stOneFont);
	
}

void CDialupKitDlg::GetSpeed()
{
	CString StrTx,StrRx,StrTemp;
	RAS_STATS statistics;
	memset(&statistics, 0, sizeof(RAS_STATS));
	statistics.dwSize = sizeof(RAS_STATS);

	// get connection statistics
	RasGetConnectionStatistics(CController::Instance()->GetRASConnHandle(), &statistics);

	// Update Tx and Rx
	Transmit = statistics.dwBytesXmited;
	Receive = statistics.dwBytesRcved;
	m_tsDuration = CTimeSpan(0, 0, 0, statistics.dwConnectDuration / 1000);
	
	StrTx = FormatNumberPointedOff(Transmit);
	StrRx = FormatNumberPointedOff(Receive);
	StrTemp = StrTx + _T("/") + StrRx;
	CStaticSetText(IDC_SPEED, StrTemp);
}

CString CDialupKitDlg::FormatNumberPointedOff(int n)
{
	CString str;
	
	if (n > (1024*1024*1024)) //G
	{
		str.Format(_T("%d%s"), n/(1024*1024),_T("Gb"));
	}
	else if (n > (1024*1024)) //M
	{
		str.Format(_T("%d%s"), n/1024,_T("mb"));
	}
	else if (n > 1024) //K
	{
		str.Format(_T("%d%s"), n,_T("kb"));
	}
	else
	{
		if (n <= 0)
			n = 0;
		
		str.Format(_T("%d%s"), n,_T("b"));
		return str;
	}
	
	
	int len = str.GetLength();
	if (len > 5) 
		str.Insert(len - 5, _T('.')); 
	
	TRACE(_T("FormatNumberPointedOff(%d): %s\n"), n, str);
	return str;
}

void CDialupKitDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(TIMER_DETECT_CONNECTION_STATUS == nIDEvent)
	{
		GetSpeed();

		if (!CController::Instance()->IsConnectionActive())
		{
			KillTimer(TIMER_DETECT_CONNECTION_STATUS);
			m_Dialstatus = STATUS_IDLE;

			CControlData::Instance()->Is8512ModemConnect();

			if (gModemConnect == FALSE)
			{
				m_bControl.SetBitmaps(IDB_BITMAP_NOUSB,RGB(255,0,255),NULL,0);
				CStaticSetText(IDC_STATE, GetText(T_USERDO1));
			}
			else
			{
				m_bControl.SetBitmaps(IDB_BITMAP_CONNECT,RGB(255,0,255),NULL,0);
				CStaticSetText(IDC_STATE, GetText(T_USERDO2));
			}
			
			if (TrayIcon != IDI_NOCON)
			{
				SetTrayIcon(3, TrayIcon);
				SetTrayIcon(1, IDI_NOCON);
			}	
		}
	}
	else if(TIMER_OPEN_COM == nIDEvent)
	{
		//插拔USB设备时的界面切换放在这里处理?
		CControlData::Instance()->Is8512ModemConnect();
		if (gModemConnect == FALSE)
		{
			m_Dialstatus = STATUS_IDLE;
			m_bControl.SetBitmaps(IDB_BITMAP_NOUSB,RGB(255,0,255),NULL,0);
			CStaticSetText(IDC_STATE, GetText(T_USERDO1));
			CStaticSetText(IDC_CONNSTATU2, GetText(T_NOCONNECT));
			CStaticSetText(IDC_CONNSTATU3, GetText(T_DISCONNECT));
			CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_NOSINGE));
			CStaticSetText(IDC_SPEED,L"0kb/0kb");
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL0)));
			PhoneStatus = 1;
			if (TrayIcon != IDI_NOCON)
			{
				SetTrayIcon(3, TrayIcon);
				SetTrayIcon(1, IDI_NOCON);
			}
			KillTimer(TIMER_DETECT_CONNECTION_STATUS);
			KillTimer(TIMER_DIALUP);
			KillTimer(TIMER_CAN_DIALUP);
		}
		else
		{
			if (!CController::Instance()->IsConnectionActive())
			{
				m_bControl.SetBitmaps(IDB_BITMAP_CONNECT,0xFFFFFF00,NULL,0);

				if (m_Dialstatus == STATUS_IDLE)
					CStaticSetText(IDC_STATE, GetText(T_USERDO2));
				
				CStaticSetText(IDC_CONNSTATU2, GetText(T_CONNECTUSB));
				CStaticSetText(IDC_CONNSTATU3, GetText(T_DEVICEDET));
				if (TrayIcon != IDI_NOCON)
				{
					SetTrayIcon(3, TrayIcon);
					SetTrayIcon(1, IDI_NOCON);
				}
			}
			else
			{
				m_Dialstatus = STATUS_CONNECT;
				SetTimer(TIMER_DETECT_CONNECTION_STATUS, DETECT_CONNECTION_ELAPSE, NULL);
				m_bControl.SetBitmaps(IDB_BITMAP_DISCONNECT,0xFFFFFF00,NULL,0);
				CStaticSetText(IDC_STATE, GetText(T_USERDO4));
				CStaticSetText(IDC_CONNSTATU2, GetText(T_CONNECTUSB));
				CStaticSetText(IDC_CONNSTATU3, GetText(T_DEVICECONNECTED));
				if (TrayIcon != IDI_NEDIUM)
				{
					SetTrayIcon(3, TrayIcon);
					SetTrayIcon(1, IDI_NEDIUM);
				}
			}
		}

		
		m_bControl.DrawTransparent(TRUE);
		m_bControl.DrawBorder(FALSE,FALSE);	

		//开COM口
		OpenCom();
	}
	else if(TIMEE_RELEASE_TMEP == nIDEvent)
	{
		ReleaseTemp();
	}
	else if(TIMER_START_QUEST == nIDEvent)
	{
		//充电模式，不停的在后台发送消息查询
		int iSize=12,pCount;

		m_pRS232->Send((BYTE *)"CANSTART", iSize, &pCount);
	}
	else if(TIMER_CAN_DIALUP == nIDEvent)
	{
		CStatusDlg dlgStatus;

		KillTimer(TIMER_CAN_DIALUP);
		KillTimer(TIMER_DETECT_CONNECTION_STATUS);		
		m_Dialstatus = STATUS_IDLE;

		CControlData::Instance()->Is8512ModemConnect();
		if (gModemConnect == FALSE)
		{
			m_bControl.SetBitmaps(IDB_BITMAP_NOUSB,RGB(255,0,255),NULL,0);
			CStaticSetText(IDC_STATE, GetText(T_USERDO1));
		}
		else
		{
			m_bControl.SetBitmaps(IDB_BITMAP_CONNECT,RGB(255,0,255),NULL,0);
			CStaticSetText(IDC_STATE, GetText(T_USERDO2));
		}
		if (TrayIcon != IDI_NOCON)
		{
			SetTrayIcon(3, TrayIcon);
			SetTrayIcon(1, IDI_NOCON);
		}

		dlgStatus.DoModal();

	}
	else if (TIMER_DIALUP== nIDEvent)
	{
		m_Dialstatus = STATUS_BEGINCONNECT;
		CController::Instance()->BeginConnect();
		KillTimer(TIMER_DIALUP);
	}
}

void CDialupKitDlg::OnClose() 
{
	int a;
	
	//CDialog::OnCancel();
	if (CController::Instance()->IsConnectionActive())
	{
		a = MessageBox(GetText(T_EXITANDDISCONNECT), NULL, MB_YESNO);
		if (a == IDYES)
		{
			Disconnect();
			CDialog::OnClose();
			return;
		}
		else if (a == IDNO)
		{
			ShowWindow(SW_HIDE);
			return;
		}
	}
	CDialog::OnClose();
}

void CDialupKitDlg::OnFontChange()
{

}

void CDialupKitDlg::Disconnect()
{
	CString str;
	
	// Kill the timer
	KillTimer(TIMER_DETECT_CONNECTION_STATUS);
	
	CStaticSetText(IDC_STATE, GetText(T_LIST_GPRS));
	
	m_Dialstatus = STATUS_DISCONNECTING;
	// stop connect if necessary
	CController::Instance()->StopConnect();
	
	// Write a history record to the registry
	/*if(Transmit != 0 && Receive !=0)
	{
		WriteOneHistoryRecord(m_tsDuration, Transmit, Receive);
	}*/
	m_Dialstatus = STATUS_IDLE;
	Transmit = Receive = 0;

	m_bControl.SetBitmaps(IDB_BITMAP_CONNECT,RGB(255,0,255),NULL,0);
	CStaticSetText(IDC_STATE, GetText(T_USERDO2));
	CStaticSetText(IDC_SPEED,L"0kb/0kb");

	if (TrayIcon != IDI_NOCON)
	{
		SetTrayIcon(3, TrayIcon);
		SetTrayIcon(1, IDI_NOCON);
	}
}

//删除安装使用的临时文件，发送scsi
void CDialupKitDlg::ReleaseTemp()
{
	CString   csNewFolder = L"C:\\IM2 TEMP";
	CString szFile,szPath;
	int mPos;

	//删除临时文件
	/*if (PathFileExists(csNewFolder))
	{
		  SHFILEOPSTRUCT   fileop;   
		    
		  fileop.hwnd           =   NULL;   
		  fileop.wFunc         =   FO_DELETE;   
		  fileop.pFrom         =   L"c:\\IM2 TEMP\0";   //注意，后面需要有一个\0字符   
		  fileop.pTo             =   NULL;   
		  fileop.fFlags       =   FOF_SILENT   |   FOF_NOCONFIRMATION;   
		  SHFileOperation(&fileop);
	}*/

	//没检测到modem的情况下，发送SCSI
	if (gModemConnect == FALSE)
	{
		GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
		szPath.ReleaseBuffer ();
		mPos=szPath.ReverseFind ('\\');
		szPath=szPath.Left (mPos);
		szFile = szPath + _T("\\SendSCSI.exe");
		if (PathFileExists(szFile))
		{
			ShellExecute(NULL,NULL,szFile,NULL,NULL,SW_SHOWMINIMIZED);
		}
	}

	KillTimer(TIMEE_RELEASE_TMEP);

}

void CDialupKitDlg::SetLangueAndApn(CString StrCountry)
{
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();

	// According to pApp->m_nLandId(read from regedit) 
	//read value from db
	CString szFile,szPath;
	TCHAR pReturn[255] = {0};


	if (wcscmp(StrCountry,L"Italy") == 0)
	{
		pApp->m_nLandId = 1;
	}
	else if (wcscmp(StrCountry,L"French") == 0)
	{
		pApp->m_nLandId = 2;
	}
	else if ((wcscmp(StrCountry,L"Germany") == 0) || (wcscmp(StrCountry,L"Austria") == 0))
	{
		pApp->m_nLandId = 3;
	}
	else if (wcscmp(StrCountry,L"Denmark") == 0)
	{
		pApp->m_nLandId = 4;
	}
	else if (wcscmp(StrCountry,L"Sweden") == 0)
	{
		pApp->m_nLandId = 5;
	}
	else if (wcscmp(StrCountry,L"China") == 0)
	{
		pApp->m_nLandId = 6;
	}
	else if (wcscmp(StrCountry,L"HongKong") == 0)
	{
		pApp->m_nLandId = 7;
	}
	else
	{
		pApp->m_nLandId = 0;
	}
	pApp->m_pResConverion->SetLanguage((TLanguage)pApp->m_nLandId);

	if (gModemConnect == FALSE)
	{
		CStaticSetText(IDC_STATE,GetText(T_USERDO1));
		CStaticSetText(IDC_CONNSTATU2, GetText(T_NOCONNECT));
		CStaticSetText(IDC_CONNSTATU3, GetText(T_DISCONNECT));
	}
	else
	{
		if (!CController::Instance()->IsConnectionActive())
		{
			CStaticSetText(IDC_STATE,GetText(T_USERDO2));
			CStaticSetText(IDC_CONNSTATU2, GetText(T_CONNECTUSB));
			CStaticSetText(IDC_CONNSTATU3, GetText(T_DEVICEDET));
		}
		else
		{
			CStaticSetText(IDC_STATE,GetText(T_USERDO4));
			CStaticSetText(IDC_CONNSTATU2, GetText(T_CONNECTUSB));
			CStaticSetText(IDC_CONNSTATU3, GetText(T_DEVICECONNECTED));
		}
	}
	
	CStaticSetText(IDC_BUTTON1, GetText(T_USAGE));
	CStaticSetText(IDC_SETTING, GetText(T_SETTING));
	CStaticSetText(IDC_HELP, GetText(T_HELP));
	CStaticSetText(IDC_CONNSTATU1, GetText(T_HANDSSTATUS));
	CStaticSetText(IDC_SIGNAL1, GetText(T_SINGE));
	CStaticSetText(IDC_BUTTON_USAGE, GetText(T_USAGE));

	if (PhoneStatus == 1)
		CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_NOSINGE));
	else if (PhoneStatus == 2)
		CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_GSM));
	else if (PhoneStatus == 3 || PhoneStatus == 4)
		CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_3G));

	//apn等设置
	if (NULL == pApp->m_pDialPram)
		pApp->m_pDialPram = new DIAL_PRAM;

	pApp->m_pDialPram->szCountry = _tcsdup(StrCountry);

	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\APNINFO.ini");

	 
	CString szKey;
	_variant_t Value;

	GetPrivateProfileString(StrCountry,L"APNName",L"1",pReturn,255,szFile);
	pApp->m_pDialPram->szAPN = _wcsdup(pReturn);

	GetPrivateProfileString(StrCountry,L"PhoneNumber",L"1",pReturn,255,szFile);
	pApp->m_pDialPram->szPhoneNumber = _wcsdup(pReturn);

	GetPrivateProfileString(StrCountry,L"UserName",L"1",pReturn,255,szFile);
	pApp->m_pDialPram->szUserName = _wcsdup(pReturn);

	GetPrivateProfileString(StrCountry,L"Password",L"1",pReturn,255,szFile);
	pApp->m_pDialPram->szPassword = _wcsdup(pReturn);

	GetPrivateProfileString(StrCountry,L"HomePage",L"1",pReturn,255,szFile);
	pApp->m_pDialPram->szHomepage = _wcsdup(pReturn);

	CString strIPV4 = _T("AT+CGDCONT=1,\"IP\",\"" );//"AT+CGDCONT=1,\"IP\",\"cmwap\"";
	strIPV4  += pApp->m_pDialPram->szAPN;
	strIPV4  += _T("\"");
	TRACE(strIPV4);
	TRACE(_T("\n"));

	CControlData::Instance()->SetAPN8709(strIPV4);	
	CControlData::Instance()->WriteNetSettings();
}

//此函数用来分析手机传过来的信息，包含msisdn，信号，网络等等
void CDialupKitDlg::GetInformation(char* pBuff)
{
	char* pTemp;
	int signal;
	
	//获取信息判断是否要显示主窗体
	if (strcmp((char *)pBuff,"YES") == 0)
	{
		KillTimer(TIMER_START_QUEST);
		if (!m_Show)
		{
			m_Show = TRUE;
			SetTrayIcon(1, IDI_NOCON);
			ShowWindow(SW_NORMAL);
		}
		m_Check = FALSE;
		
		//发送消息获取网络，信号
		{
			int iSize=12,pCount;
		
			m_pRS232->Send((BYTE *)"MODEMBEGIN", iSize, &pCount);
		}
	}
	else if (strcmp((char *)pBuff,"NO") == 0)
	{
		//这里的处理暂时为退出操作
		KillTimer(TIMER_START_QUEST);
		m_Check = FALSE;
		PostMessage(WM_QUIT, NULL, NULL);
	}
	else if (strncmp((char *)pBuff,"opco:",5) == 0)
	{
		//设置默认的语言和apn
		CString SzTemp; 
		
		KillTimer(TIMER_START_QUEST);
		SzTemp.Format(_T("%S"),((char *)(pBuff + 5)));
		SetLangueAndApn(SzTemp);
	}

	
	if(strcmp(pBuff,"NOSIGNEL") == 0)
	{
		CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_NOSINGE));
		m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL0)));
		PhoneStatus = 1;
	}
	else if(strncmp(pBuff,"MISDN",5) == 0)
	{
		char pInform[100] = {0};

		pTemp = pBuff + 6;
		if (strlen(pTemp) > 10)
		{
			strncpy(pInform,(char*)pTemp,10);
			pTemp = pBuff + 16;
			MSISDN.Format(_T("%S%S%S"), pInform,_T("\r"),pTemp);
		}
		else
			MSISDN.Format(_T("%S"), pTemp);
	}
	else if (strncmp(pBuff,"INFO:",5) == 0)
	{
		pTemp = pBuff + 5;
		
		if (strncmp(pTemp,"GSM",3) == 0)
		{
			CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_GSM));
			PhoneStatus = 2;
			pTemp = pBuff + 8;
		}
		if (strncmp(pTemp,"H3G",3) == 0)
		{
			CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_3G));
			PhoneStatus = 4;
			pTemp = pBuff + 8;
		}
		else if(strncmp(pTemp,"3G",2) == 0)
		{
			CStaticSetText(IDC_IDC_SIGNAL2, GetText(T_3G));
			PhoneStatus = 3;
			pTemp = pBuff + 7;
		}

		signal = atoi(pTemp);
		
		if (signal < 62)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL9)));
		else if (signal < 68)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL8)));
		else if (signal < 74)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL7)));
		else if (signal < 80)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL6)));
		else if (signal < 86)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL5)));
		else if (signal < 92)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL4)));
		else if (signal < 98)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL3)));
		else if (signal < 104)
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL2)));
		else 
			m_bSingnalbitmap.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_SIGNAL1)));
	
	}
}

void CDialupKitDlg::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CRgnDialog::OnWindowPosChanging(lpwndpos);

	// TODO: Add your message handler code here
	if (!m_Show)
		lpwndpos->flags&=~SWP_SHOWWINDOW;   
}

BOOL CDialupKitDlg::OnEraseBkgnd(CDC* pDC)
{
	CBitmap	   BitmapBG;
    CDC		   TmpDC;   
    CRect	   Rect;   

    BitmapBG.LoadBitmap(IDB_BITMAP1);

    TmpDC.CreateCompatibleDC(pDC);

    TmpDC.SelectObject(&BitmapBG);

    GetClientRect(&Rect);

    pDC->StretchBlt(0, 0, Rect.Width(), Rect.Height(), &TmpDC, 0, 0, 480, 267, SRCCOPY);

    BitmapBG.DeleteObject();


	return 0;

}

void CDialupKitDlg::OnBnClickedButtonInq()
{
	// TODO: Add your control notification handler code here
	ShellExecute(NULL,   _T("open"),   _T("iexplore"),   _T("www.inq.com"),   NULL,   SW_SHOWNORMAL); 
}
