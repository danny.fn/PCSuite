#ifndef __PCSUITE_ERROR_H__
#define __PCSUITE_ERROR_H__

#define  ERR_SUCCESS                  (0)      //Success
#define  ERR_FAILED                   (1)      //Failed
#define  ERR_FILENOTEXISTED           (2)      //File is not existed
#define  ERR_BADPARAM                 (3)      //Bad parametre
#define  ERR_NORESOURCE               (4)      //Have not this resource
#define  ERR_UNSUPPORTED              (5)      //Unsupported
#define  ERR_MEMORYACCESS             (6)      //Access share memory failed(maybe do not created)
#define  ERR_ACCESSCONFLICT           (7)      //Access conflict(mutex synchonorize failed)
#define  ERR_NOSHAREMEMORY            (8)      //Share memory is not existed,should create it
#define  ERR_MEMORYEXISTED            (9)      //Share memory is existed,no need to create
#define  ERR_MEMORYOVERFLOW           (10)     //overrun the max number of share memory
#define  ERR_NOMEMORY                 (11)     //Out of memory
#define  ERR_TIMEOUT                  (12)     //Operation timeout
#define  ERR_PORTOPENFAILED           (13)     //Open a port failed
#define  ERR_BADSTATE                 (14)     //Bad State
#define  ERR_NOSUCH                   (15)     //the value or the field no exist
#define  ERR_CALENDARFULL             (16)
#endif //__PCSUITE_ERROR_H__