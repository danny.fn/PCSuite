// HangingupStatusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DialupKit.h"
#include "HangingupStatusDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHangingupStatusDlg dialog


#define CLOSE_DLG_ELAPSE          1000
#define IDTIMER_CLOSE_STATUS_DLG  0

CHangingupStatusDlg::CHangingupStatusDlg(HANDLE hEvent, CWnd* pParent /*=NULL*/)
: CDialog(CHangingupStatusDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHangingupStatusDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	
	m_hEvent = hEvent;
}


void CHangingupStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHangingupStatusDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHangingupStatusDlg, CDialog)
//{{AFX_MSG_MAP(CHangingupStatusDlg)
ON_WM_TIMER()
ON_WM_CLOSE()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHangingupStatusDlg message handlers

BOOL CHangingupStatusDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//
	
    CString strDisconnecting;
    // hide the OK button
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
    strDisconnecting = pApp->g_GetText(T_LIST_GPRS,pApp->m_nLandId);
	//	PostMessage(WM_CLOSE, 0, 0);
	SetTimer(IDTIMER_CLOSE_STATUS_DLG, CLOSE_DLG_ELAPSE, NULL);
    GetDlgItem(IDC_STATIC_HANGINGUP)->SetWindowText(strDisconnecting);
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CHangingupStatusDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(IDTIMER_CLOSE_STATUS_DLG == nIDEvent)
	{
		KillTimer(IDTIMER_CLOSE_STATUS_DLG);
		// CLOSE THE DIALOG
		PostMessage(WM_CLOSE, 0, 0);
	}
	CDialog::OnTimer(nIDEvent);
}

void CHangingupStatusDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	// wait for the thread to terminate
	WaitForSingleObject(m_hEvent, INFINITE);
	CDialog::OnClose();
}
