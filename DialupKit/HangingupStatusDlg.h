#if !defined(AFX_HANGINGUPSTATUSDLG_H__EB40DF41_E51E_458C_964B_6A9EBB3EEB22__INCLUDED_)
#define AFX_HANGINGUPSTATUSDLG_H__EB40DF41_E51E_458C_964B_6A9EBB3EEB22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HangingupStatusDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHangingupStatusDlg dialog

class CHangingupStatusDlg : public CDialog
{
	// Construction
public:
	CHangingupStatusDlg(HANDLE hEvent, CWnd* pParent = NULL);   // standard constructor
	
	// Dialog Data
	//{{AFX_DATA(CHangingupStatusDlg)
	enum { IDD = IDD_DIALOG_HANGING_UP };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHangingupStatusDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	
	HANDLE m_hEvent;
	// Generated message map functions
	//{{AFX_MSG(CHanginggupStatusDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HANGINGUPSTATUSDLG_H__EB40DF41_E51E_458C_964B_6A9EBB3EEB22__INCLUDED_)
