#if !defined(AFX_HISTORYDLG_H__36B92877_FF21_4879_8A68_F9E6F9947CE2__INCLUDED_)
#define AFX_HISTORYDLG_H__36B92877_FF21_4879_8A68_F9E6F9947CE2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HistoryDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHistoryDlg dialog

#include "MyDlg.h"
/////////////////////////////////////////////////////////////////////////////
// CHistoryDlg dialog

class CHistoryDlg : public CMyDlg
{
	// Construction
public:
	CHistoryDlg(CWnd* pParent = NULL);   // standard constructor
	
	// Dialog Data
	//{{AFX_DATA(CHistoryDlg)
	enum { IDD = IDD_DIALOG_HISTORY };
	CListCtrl	m_listHistory;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHistoryDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	void TransDlg();
    void InitHistoryList();
	// Generated message map functions
	//{{AFX_MSG(CHistoryDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnResetall();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HISTORYDLG_H__36B92877_FF21_4879_8A68_F9E6F9947CE2__INCLUDED_)
