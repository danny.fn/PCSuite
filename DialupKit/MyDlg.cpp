

// MyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DialupKit.h"
#include "MyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyDlg dialog

CMyDlg::CMyDlg(UINT nIDTemplate, CWnd* pParent /*=NULL*/, BOOL bTranslateMsg)
	: CDialog(nIDTemplate, pParent)
{
	//{{AFX_DATA_INIT(CMyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

    m_bTranslateMsg = bTranslateMsg;
}

// 设置窗口标题
void CMyDlg::SetAppText(UINT uResourceID_AppName) 
{
    CString strAppName; 
    strAppName.LoadString(uResourceID_AppName); 
    SetWindowText(strAppName); 
}

// 从资源取出文本
char *CMyDlg::GetMessageString(UINT uResource) 
{
    static char szText[128];
    CString strText;
    strText.LoadString(uResource);
    strcpy(szText, (LPSTR)(LPCTSTR)strText);
    return szText;
}

// 设置控件的文本
void CMyDlg::SetWindowTitle(UINT uCtrlID, UINT uResourceID) 
{
    CString strText;
    strText.LoadString(uResourceID);
    GetDlgItem(uCtrlID)->SetWindowText(strText);
}

void CMyDlg::InitListCtrl(CImageList *pImageList, CListCtrl *pListCtrl)
{
    //pImageList->Create (IDB_NETLINK, 20, 1, ILC_COLOR8);
    pListCtrl->SetImageList (pImageList, TVSIL_NORMAL);
    pListCtrl->SetExtendedStyle(LVS_EX_FULLROWSELECT|
        LVS_EX_TRACKSELECT );
    
    CString name;

    //for (int i = 0; i < 2; i++) 
    for(int i = 1; i < 2; i++) // modified by shaozw to hide GPRS dialup icon
	{
     //   name.Format("%s", GetMessageString(IDS_NETWORD_G + i));
        pListCtrl->InsertItem(LVIF_TEXT|LVIF_STATE, i, name, 
            LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED, 0, 0);
    }
}

BEGIN_MESSAGE_MAP(CMyDlg, CDialog)
	//{{AFX_MSG_MAP(CMyDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyDlg message handlers

BOOL CMyDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ModifyStyleEx(0, WS_EX_APPWINDOW);

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	
    //SetAppText(IDS_APP_TITLE); 
    
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CMyDlg::PreTranslateMessage(MSG* pMsg) 
{
    if (!m_bTranslateMsg) {
	    if (pMsg->message == WM_KEYDOWN) {
		    char cKey = pMsg->wParam;
		    if (cKey == VK_RETURN) {
			    return TRUE;
		    } else if (cKey == VK_ESCAPE)
			    return TRUE;
	    }
   }

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CMyDlg::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
//    cs.lpszClass = CLASS_AMOI_DIALINGKIT_CLASS_NAME; 	
	return CDialog::PreCreateWindow(cs);
}
