#include "stdafx.h"
#include "RS232.h"

#define RECEIVE_BUFFER_SIZE  100
#define SEND_BUFFER_SIZE     100
#define PORT_DATA_MAX_SIZE   2048		// set the max data size for port operation.
extern 	HWND	ghWnd;
CRS232::CRS232(void)
{
    m_nPort        = 0;
    m_hComm        = NULL;

	m_TimeOut      = 4000;   //4 second
	m_WaitingTimer = 40000;  //40 second
	m_hThread      = NULL;

	memset(&m_WaitOverLapped, 0, sizeof(OVERLAPPED));
	memset(&m_ReadOverLapped, 0, sizeof(OVERLAPPED));
	memset(&m_WriteOverLapped, 0, sizeof(OVERLAPPED));
	
	m_bExitThread             = FALSE;
	m_hMutex                  = NULL;
}

CRS232::~CRS232(void)
{
	Close();
}

int CRS232::Open(int nPort,int nBaud,int nParity,int nStopbits,int nDatabits)
{
	int   nTimeout = m_TimeOut * 12;  //12 seconds
	CString str;

	// If current instance has been used, so close the port.
    Close();

	// auto reset the event
    m_WaitOverLapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_ReadOverLapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_WriteOverLapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_hMutex = CreateMutex(NULL,FALSE,NULL);
    m_nPort = nPort;

	str.Format(L"\\\\.\\COM%d", nPort);
    m_hComm = CreateFile(str,                       // 文件名
                         GENERIC_READ | GENERIC_WRITE,   //  访问模式 允许读写
                         0,                              //  共享模式 此项必须为0
                         NULL,                           // no security attrs
                         OPEN_EXISTING,                  //设置产生方式，创建方式
                         FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,           // 文件属性和标志 ：我们准备使用异步通信,我们使用了FILE_FLAG_OVERLAPPED结构。这正是使用API实现非阻塞通信的关键所在。
                         NULL);                          // 临时文件的句柄，通常为NULL

    if (m_hComm == INVALID_HANDLE_VALUE)
    {
    	m_hComm = NULL;
		Close();
        return ERR_PORTOPENFAILED;
    }


	//清干净输入,输出缓冲区
    if (!PurgeComm(m_hComm, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR))
    {
        Close();
        return ERR_PORTOPENFAILED;
    }

    if (!SetupComm(m_hComm, RECEIVE_BUFFER_SIZE, SEND_BUFFER_SIZE))  //设置输入、输出缓冲区的大小
    {
    	Close();
        return ERR_PORTOPENFAILED;
    }

    
    DCB	        	dcb;                 // 定义数据控制块结构
	memset(&dcb, 0, sizeof(DCB));
    dcb.DCBlength = sizeof(DCB);

	if (!GetCommState(m_hComm, &dcb))
	{
		Close();
		return ERR_PORTOPENFAILED;
	}

    // Set baudrate
    dcb.BaudRate = nBaud;
	dcb.ByteSize = nDatabits;
	dcb.StopBits = nStopbits;
    dcb.Parity   = nParity;
    
	dcb.fBinary           = TRUE;
    dcb.fOutxCtsFlow 	  = FALSE;
    dcb.fOutxDsrFlow      = FALSE;
    dcb.fDtrControl       = DTR_CONTROL_DISABLE;
    dcb.fRtsControl 	  = RTS_CONTROL_DISABLE;
    dcb.fDsrSensitivity   = FALSE;
    dcb.fTXContinueOnXoff = FALSE;
    dcb.fOutX 		      = FALSE;
    dcb.fInX 		      = FALSE;

    if (!SetCommState(m_hComm, &dcb))    //串口参数配置
    {
        Close();
        return ERR_PORTOPENFAILED;
    }

	if (SetTimeOut(m_TimeOut) != ERR_SUCCESS)
	{
		Close();
        return ERR_PORTOPENFAILED;
	}

	//EV_BREAK | EV_CTS | EV_DSR | EV_ERR | EV_RING | EV_RLSD | EV_RXCHAR | EV_RXFLAG | EV_TXEMPTY
	if(!SetCommMask(m_hComm,EV_RXCHAR))
	{
		Close();
		return ERR_PORTOPENFAILED;
	}

	DWORD   dwThreadID;

	m_bExitThread = FALSE;
	m_hThread = CreateThread(NULL, 0, SerialThreadProc, this, 0, &dwThreadID);

	return ERR_SUCCESS;
}

int CRS232::Close(void)
{
	m_bExitThread = TRUE;	//Exit the thread

	if (NULL != m_hThread)
	{
		CloseHandle(m_hThread);
		m_hThread = 0;
	}

	if (NULL != m_hComm)
	{
		PurgeComm(m_hComm, PURGE_RXCLEAR | PURGE_TXCLEAR);
		SetCommMask(m_hComm, 0 );  // Forces the WaitCommEvent to return
		CloseHandle(m_hComm);
		m_hComm = NULL;
	}

	if (m_WaitOverLapped.hEvent != NULL)
	{
		CloseHandle(m_WaitOverLapped.hEvent);
		m_WaitOverLapped.hEvent = NULL;
	}

	if (m_ReadOverLapped.hEvent != NULL)
	{
		CloseHandle(m_ReadOverLapped.hEvent);
		m_ReadOverLapped.hEvent = NULL;
	}

	if (m_WaitOverLapped.hEvent != NULL)
	{
		CloseHandle(m_WaitOverLapped.hEvent);
		m_WriteOverLapped.hEvent = NULL;
	}

	if(m_hMutex != NULL)
	{
		CloseHandle(m_hMutex);
		m_hMutex = NULL;
	}
	return ERR_SUCCESS;
}

int CRS232::Send(BYTE *pBuff, int iSize, int *pCount)
{
    DWORD   BytesSent = 0;
	DWORD   dwErrCode = 0;
	DWORD   dwError  = 0;


	if (iSize > PORT_DATA_MAX_SIZE)
		return ERR_MEMORYOVERFLOW;

	if (pCount != NULL)
		*pCount = 0;

	if (iSize == 0)
		return ERR_SUCCESS;

	if (m_hComm == NULL)
		return ERR_NORESOURCE;

/*
	if(WaitForSingleObject(m_hMutex,10000) != WAIT_OBJECT_0)
	{
		TRACE("WaitForSingleObject failed.\n");
		return ERR_FAILED;
	}
*/

    if(ClearCommError(m_hComm, &dwErrCode, NULL) && dwErrCode > 0)	//清除错误
	{
		TRACE("ClearCommError = %d\n",dwErrCode);
		if(dwErrCode == ERROR_GEN_FAILURE)
		{
			PurgeComm(m_hComm, PURGE_TXABORT | PURGE_TXCLEAR);	
		}
	}

    if (!WriteFile(m_hComm,	// Handle to COMM Port ,                         文件句柄
                   pBuff,	// Pointer to message buffer in calling finction 写缓冲区
                   iSize,	// Length of message to send                     要求写出的字节数
                   &BytesSent,	// Where to store the number of bytes sent   实际写出的字节数
                   &m_WriteOverLapped))	// Overlapped structure              指向一个OVERLAPPED结构
    {
		dwErrCode = GetLastError();
		
		if (dwErrCode != ERROR_IO_PENDING)
			return ERR_FAILED;

		FlushFileBuffers(m_hComm);
		
		// wait for operation complete
		DWORD Ret = WaitForSingleObject(m_WriteOverLapped.hEvent, m_WaitingTimer);

		PurgeComm(m_hComm, PURGE_TXABORT | PURGE_TXCLEAR);

		if (Ret == WAIT_FAILED)
 			return ERR_FAILED;
		if (Ret == WAIT_TIMEOUT)
			return ERR_TIMEOUT;
		if (Ret == WAIT_ABANDONED)
			return ERR_FAILED;
		if (Ret == WAIT_OBJECT_0)
			ResetEvent(m_WriteOverLapped.hEvent);
    }

	if (pCount != NULL)
	    *pCount = m_WriteOverLapped.InternalHigh;

    return ERR_SUCCESS;
}

int CRS232::Receive(BYTE *pBuff, int iSize, int *pCount)
{
    long      lMSecsExpired = 0L;
    DWORD     lHasRead = 0;
	COMSTAT   ComState;
	DWORD     dwError  = 0;

	if (iSize > PORT_DATA_MAX_SIZE)
		return ERR_MEMORYOVERFLOW;

	if (pCount != NULL)
		*pCount = 0;

	if (iSize == 0)
		return ERR_SUCCESS;

    if (NULL == m_hComm)
        return ERR_NORESOURCE;

/*
	if(WaitForSingleObject(m_hMutex,10000) != WAIT_OBJECT_0)
	{
		TRACE("WaitForSingleObject failed.\n");
		return ERR_FAILED;
	}
*/

	if(ClearCommError(m_hComm,&dwError,&ComState) && dwError > 0)
	{
		//清除输入缓冲
		//PurgeComm(m_hComm, PURGE_RXABORT | PURGE_RXCLEAR);
	}

	if(ComState.cbInQue == 0)
	{
		return ERR_FAILED;
	}

    if (!ReadFile(m_hComm, pBuff, ComState.cbInQue, &lHasRead, &m_ReadOverLapped))
    {
		dwError = GetLastError();

        if(dwError != ERROR_IO_PENDING)
			return ERR_FAILED;

		// wait for operation complete
		DWORD Ret = WaitForSingleObject(m_ReadOverLapped.hEvent, m_WaitingTimer); //1 minute

		if (Ret == WAIT_FAILED)
 			return ERR_FAILED;
		if (Ret == WAIT_TIMEOUT)
			return ERR_TIMEOUT;
		if (Ret == WAIT_ABANDONED)
			return ERR_FAILED;
		if (Ret == WAIT_OBJECT_0)
			ResetEvent(m_ReadOverLapped.hEvent);
    }

	if (pCount != NULL)
	    *pCount = m_ReadOverLapped.InternalHigh;
    
    return ERR_SUCCESS;
}

int CRS232::ClearBuffer(void)
{
	if (m_hComm == NULL)
		return ERR_FAILED;

    PurgeComm(m_hComm, PURGE_RXCLEAR | PURGE_TXCLEAR);

    if (FlushFileBuffers(m_hComm) == 0)
    {
        return ERR_FAILED;
    }

    return ERR_SUCCESS;
}

int CRS232::SetTimeOut(int Millisecond)
{
	int Result = ERR_SUCCESS;
	COMMTIMEOUTS Timeout;

	if (m_hComm == NULL)
		return ERR_FAILED;

	m_TimeOut = Millisecond;

    if (!GetCommTimeouts(m_hComm, &Timeout))
    {
        Result = ERR_FAILED;
    }
    else
    {
        Timeout.ReadIntervalTimeout         = 0xFFFFFFFF;//m_TimeOut;   // 读间隔超时
        Timeout.ReadTotalTimeoutMultiplier  = 0;//m_TimeOut;   // 读时间系数
        Timeout.ReadTotalTimeoutConstant    = m_TimeOut;   // 读时间常量
		
        Timeout.WriteTotalTimeoutMultiplier = 10;//m_TimeOut;   // 写时间系数
        Timeout.WriteTotalTimeoutConstant   = m_TimeOut;   // 写时间常量
    }

    if (Result == ERR_SUCCESS && !SetCommTimeouts(m_hComm, &Timeout))         //设置读写操作所允许的超时
        Result = ERR_FAILED;

	return Result;
}

int CRS232::RegisteCallBack(PFNRS232CALLBACK pfnCallBack, void* pUserData)
{
	m_pUserData    = pUserData;
	m_pfnCallback  = pfnCallBack;

	return ERR_SUCCESS;
}

BOOL CRS232::IsOpen()
{
	return m_hComm != NULL ? TRUE : FALSE;
}

DWORD CRS232::ThreadProc()
{
	DWORD      dwTrans;
	DWORD      EvtMask  = 0;
	BOOL       bResult = FALSE;
	COMSTAT    Stat;
	DWORD      dwError;

	while(!m_bExitThread)
	{
		if((bResult = WaitCommEvent(m_hComm, &EvtMask,&m_WaitOverLapped)) == FALSE)
		{
			if(GetLastError() == ERROR_IO_PENDING)
			{
				GetOverlappedResult(m_hComm,&m_WaitOverLapped,&dwTrans,TRUE);
			}
			else
			{
				continue;
			}
		}
		
		if(EvtMask & EV_ERR) // == EV_ERR
			::ClearCommError(m_hComm, &dwError, &Stat);

		if(EvtMask & EV_RXCHAR)
		{
			/*if(m_pfnCallback)
			{
				m_pfnCallback(m_pUserData, m_nPort);
			}*/
			PostMessage(ghWnd, WM_COMMNOTIFY, EV_RXCHAR, 0);
		}
/*
		switch(EvtMask&EV_RXCHAR)
		{
		case EV_RXCHAR: //EV_RXCHAR:A character was received and placed in the input buffer.
			if(m_pfnCallback)
			{
				m_pfnCallback(m_pUserData, m_nPort);
			}
			break;
		case EV_TXEMPTY://EV_TXEMPTY The last character in the output buffer was sent   
			break;
		case EV_CTS:    //EV_CTS The CTS (clear-to-send) signal changed state.    
			break;
		case EV_DSR:    //EV_DSR The DSR (data-set-ready) signal changed state.    
			break;
		case EV_RING:   //EV_RING A ring indicator was detected.    
			break;
		case EV_RLSD:   //EV_RLSD The RLSD (receive-line-signal-detect) signal changed state.    
			break;
		case EV_BREAK:  //EV_BREAK A break was detected on input.  
			break;
		case EV_ERR:    //EV_ERR A line-status error occurred. Line-status errors are CE_FRAME, CE_OVERRUN, and CE_RXPARITY.    
			break;
		default:
			continue;
		}
*/
	}

	return ERR_SUCCESS;
}

DWORD WINAPI CRS232::SerialThreadProc(LPVOID lpParam)
{
	CRS232 *pThis = (CRS232*)lpParam;
	return pThis->ThreadProc();
}
