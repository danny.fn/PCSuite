// RasEntry.cpp: implementation of the CRasEntry class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "RasEntry.h"

#include  <raserror.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define RASENTRY_AMOI_MOBILE    "INQ1 USB Modem"
#define RASPHONE_DIRECTORY      "Documents and Settings\\All Users\\Application Data\\Microsoft\\Network\\Connections\\Pbk\\rasphone.pbk"
#define STR_SUBKEY_COMNAME_AMOI "SYSTEM\\CurrentControlSet\\Enum\\USB\\Vid_1614&Pid_0405&Amoi-A500"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRasEntry::CRasEntry()
{

}

CRasEntry::~CRasEntry()
{

}

//  Retrieves country-specific dialing information from the Windows Telephony list of countries.
//  pass the Country ID (e.g. 1 = USA).    
//  Returns country code for the country identified by the  dwCountryID.
DWORD  CRasEntry::GetCountryInfo(DWORD dwCID, RASCTRYINFO &RasCTryInfo, char *szCountryName)
{
    LPRASCTRYINFO lpRasCTryInfo = NULL;
    DWORD cb = sizeof(RASCTRYINFO);
    DWORD dwBufferSize = 0;
    DWORD dwRet        = 0;  
    DWORD dwCountryID  = 0;
    
    lpRasCTryInfo = (LPRASCTRYINFO)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, cb);
    if(lpRasCTryInfo == NULL)
        return 0;
    
    lpRasCTryInfo->dwSize = sizeof(RASCTRYINFO);
    lpRasCTryInfo->dwCountryID = dwCID;

    dwRet = RasGetCountryInfo(lpRasCTryInfo, &dwBufferSize);
    
    if (dwRet == ERROR_BUFFER_TOO_SMALL)
    {
        if (HeapFree(GetProcessHeap(), 0, (LPVOID)lpRasCTryInfo))  //  Free  initial  buffer
        {
            //  And  reassign  a  new  buffer  with  the  value  returned  in  dwBufferSize
            lpRasCTryInfo = (LPRASCTRYINFO)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwBufferSize);
            if  (NULL == lpRasCTryInfo)
                return  0;

            lpRasCTryInfo->dwSize = sizeof(RASCTRYINFO);
            lpRasCTryInfo->dwCountryID = dwCID;
            
            //  Again  call  RasGetCountryInfo
            dwRet = RasGetCountryInfo(lpRasCTryInfo, &dwBufferSize);
            if  (dwRet == ERROR_SUCCESS)
            {
                //  Store  the  necessary  info.
                ::memcpy(&RasCTryInfo, lpRasCTryInfo, sizeof(RASCTRYINFO));
                ::strcpy(szCountryName, (char *)lpRasCTryInfo + lpRasCTryInfo->dwCountryNameOffset);
                dwCountryID = lpRasCTryInfo->dwCountryID;
            }

            HeapFree(GetProcessHeap(), 0, (LPVOID)lpRasCTryInfo);
        }  
        else  
            return 0;
    }

    return  dwCountryID;
}

BOOL  CRasEntry::CreateRasEntry(CString strEntryName, RASENTRY &RasEntry)  
{  
    LPRASENTRY lpRasEntry = NULL;
    DWORD cb = sizeof(RASENTRY);
    DWORD dwRet        = 0;
    DWORD dwBufferSize = 0;

    //  This  is  important!  Find  the  buffer  size  (different  from  sizeof(RASENTRY)).
    RasGetEntryProperties(NULL, L"", NULL, &dwBufferSize, NULL, NULL);
    if (dwBufferSize == 0)
        return FALSE;

    lpRasEntry = (LPRASENTRY)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwBufferSize);
    if (lpRasEntry == NULL)
        return FALSE;

    lpRasEntry->dwSize = dwBufferSize;
    lpRasEntry->dwfOptions = RASEO_RemoteDefaultGateway | RASEO_ModemLights | RASEO_IpHeaderCompression | RASEO_SwCompression | RASEO_NetworkLogon;   
    //dwfoptions有设置ip，ppp才可以正常使用
//    lpRasEntry->dwCountryID = RasEntry.dwCountryID;
//    lpRasEntry->dwCountryCode = RasEntry.dwCountryCode;
//    ::strcpy(lpRasEntry->szAreaCode, RasEntry.szAreaCode);
//    ::strcpy(lpRasEntry->szLocalPhoneNumber, RasEntry.szLocalPhoneNumber);
    // out of memory ? ? ?
    //::memcpy(&lpRasEntry->ipaddr, &RasEntry.ipaddr, sizeof(CIPAddressCtrl));
    //::memcpy(&lpRasEntry->ipaddrDns, &RasEntry.ipaddrDns, sizeof(CIPAddressCtrl));
    //::memcpy(&lpRasEntry->ipaddrDnsAlt, &RasEntry.ipaddrDnsAlt, sizeof(CIPAddressCtrl));
    //::memcpy(&lpRasEntry->ipaddrWins, &RasEntry.ipaddrWins, sizeof(CIPAddressCtrl));  
    //::memcpy(&lpRasEntry->ipaddrWinsAlt, &RasEntry.ipaddrWinsAlt, sizeof(CIPAddressCtrl)); 
    lpRasEntry->dwfNetProtocols = RASNP_Ip;//RASNP_NetBEUI;
    lpRasEntry->dwFramingProtocol = RASFP_Ppp;// RASFP_Ras;//
    ::_tcscpy(lpRasEntry->szDeviceType, RASDT_Modem);
    ::_tcscpy(lpRasEntry->szDeviceName, RasEntry.szDeviceName);

    dwRet = RasSetEntryProperties(NULL, strEntryName.GetBuffer(strEntryName.GetLength()),
               lpRasEntry, dwBufferSize, NULL, 0);

	// ADD BY SHAOZW
	strEntryName.ReleaseBuffer();

    HeapFree(GetProcessHeap(), 0, (LPVOID)lpRasEntry);

    if (dwRet == 0)  
        return  TRUE;  
    else  
        return  FALSE;
}
 
//  Retrieves  all  the  available  RAS-capable  devices.  
//  Specify  the  device  type  in  szDeviceType  from  one  of  these:  
//  RASDT_Modem,  rasDT_Isdn,  rasDT_X25,  rasDT_Vpn,  rasDT_Pad,  rasDT_Generic,    
//  rasDT_Serial,  rasDT_FrameRelay,  rasDT_Atm,  rasDT_Sonet,  rasDT_SW56,    
//  rasDT_Irda,  rasDT_Parallel,  rasDT_PPPoE  
//  
//  This  returns  name  of  all  the  available  devices  in  strDevArray.  
//  If  there  are  no  devices  available,  it  returns  FALSE.  
//BOOL  CRasEntry::EnumModem(char *szDeviceType, CStringArray &strDevArray)
BOOL  CRasEntry::EnumModem(TCHAR *szDeviceType, CStringArray &strDevArray)
{  
    DWORD i                     = 0;
    DWORD dwRet                 = 0;
    DWORD dwcb                  = 0;
    DWORD dwDevices             = 0;
    LPRASDEVINFO lpRasDevInfo   = NULL;
    BOOL bRet                   = FALSE;
    
    //  allocate  buffer  for  one  device  
    dwcb = sizeof(RASDEVINFO);
    lpRasDevInfo = (LPRASDEVINFO)malloc((UINT)dwcb);
    lpRasDevInfo->dwSize = dwcb;
    
    //  RasEnumDevices
    dwRet = RasEnumDevices(lpRasDevInfo,  &dwcb,  &dwDevices);
    if (dwRet == ERROR_BUFFER_TOO_SMALL && dwDevices != 0)
    {
        //  found  more  then  one  device(s)
        lpRasDevInfo = (LPRASDEVINFO)realloc((VOID*)lpRasDevInfo, (UINT)dwcb);
        for (i = 0; i < dwDevices; i++)
        {  
            lpRasDevInfo[i].dwSize  =  sizeof(RASDEVINFO);
        }  
        dwRet = RasEnumDevices(lpRasDevInfo, &dwcb, &dwDevices);
    }
    
    if(dwRet == 0)    
    {
        for (i = 0; i < dwDevices; i++)
        {
            if(_tcsncicmp(lpRasDevInfo[i].szDeviceType, szDeviceType, _tcslen(lpRasDevInfo[i].szDeviceType)) == 0)
            {
                strDevArray.Add(lpRasDevInfo[i].szDeviceName);
                //TRACE(L"%s\n", lpRasDevInfo[i].szDeviceName);
            }
        }
    }
    free((VOID*)lpRasDevInfo);
    
    return  bRet;
} 
 
//  Saves  the  user  info  (username,  password)  
//  Set  bRemovePassword  to  TRUE  if  you  don't  want  the  password  to  be  saved.  
//BOOL  CRasEntry::SetEntryDialParams(CString strEntryName, CString strUsername, CString strPassword, BOOL bRemovePassword)
BOOL  CRasEntry::SetEntryDialParams(CString strEntryName, CString strUsername, CString strPassword, CString strPhoneNumber, BOOL bRemovePassword) // MODIFIED BY SHAOZW TO ADD PHONE NUMBER PARAMETER
{  
    RASDIALPARAMS  rdParams;
    ZeroMemory(&rdParams, sizeof(RASDIALPARAMS));
    rdParams.dwSize = sizeof(RASDIALPARAMS);
    ::_tcscpy(rdParams.szEntryName, strEntryName.GetBuffer(strEntryName.GetLength()));
    ::_tcscpy(rdParams.szUserName, strUsername.GetBuffer(strUsername.GetLength()));
    ::_tcscpy(rdParams.szPassword, strPassword.GetBuffer(strPassword.GetLength()));
	// ADD BY SHAOZW
	::_tcscpy(rdParams.szPhoneNumber, strPhoneNumber.GetBuffer(strPhoneNumber.GetLength()));

    DWORD dwRet = RasSetEntryDialParams(NULL,  &rdParams,  bRemovePassword);

    if (dwRet == 0)
        return TRUE;
    else
        return FALSE;
}


BOOL  CRasEntry::IsEntryExist(CString strEntryName)  
{  
    LPRASENTRY lpRasEntry = NULL;
    DWORD cb = sizeof(RASENTRY);
    DWORD dwRet        = 0;
    DWORD dwBufferSize = 0;

    //  This  is  important!  Find  the  buffer  size  (different  from  sizeof(RASENTRY)).
    dwRet = RasGetEntryProperties(NULL, strEntryName, NULL, &dwBufferSize, NULL, NULL);

	return (dwRet != ERROR_CANNOT_FIND_PHONEBOOK_ENTRY);



//    if (dwBufferSize == 0)
//        return FALSE;
//
//    lpRasEntry = (LPRASENTRY)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwBufferSize);
//    if (lpRasEntry == NULL)
//        return FALSE;
//
//    lpRasEntry->dwSize = dwBufferSize;
//
//	
//
//    HeapFree(GetProcessHeap(), 0, (LPVOID)lpRasEntry);
//

}

