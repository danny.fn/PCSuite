// RasEntry.h: interface for the CRasEntry class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RASENTRY_H__48A298DB_E928_4E1E_9EDC_231A4D3D829C__INCLUDED_)
#define AFX_RASENTRY_H__48A298DB_E928_4E1E_9EDC_231A4D3D829C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include  <ras.h> 

class CRasEntry  
{
public:
	CRasEntry();
	virtual ~CRasEntry();

    BOOL  SetEntryDialParams(CString strEntryName, CString strUsername, CString strPassword, CString strPhoneNumber, BOOL bRemovePassword);  
    BOOL  EnumModem(TCHAR *szDeviceType, CStringArray &strDevArray);  
    BOOL  CreateRasEntry(CString  strEntryName,  RASENTRY  &RasEntry);  
    DWORD GetCountryInfo(DWORD dwCID, RASCTRYINFO &RasCTryInfo,  char *szCountryName); 

	BOOL  IsEntryExist(CString strEntryName); // ADD BY SHAOZW

};

#endif // !defined(AFX_RASENTRY_H__48A298DB_E928_4E1E_9EDC_231A4D3D829C__INCLUDED_)
