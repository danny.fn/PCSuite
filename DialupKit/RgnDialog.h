#if !defined(AFX_RGNDIALOG_H__B66D3B02_85AE_41CB_9531_8528481F6428__INCLUDED_)
#define AFX_RGNDIALOG_H__B66D3B02_85AE_41CB_9531_8528481F6428__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RgnDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRgnDialog dialog

class CRgnDialog : public CDialog
{
// Construction
public:
	CRgnDialog(UINT nIDTemplate, CWnd* pParentWnd = NULL);
	~CRgnDialog();

	BOOL CreateRgnFromBitmap(HBITMAP hBitmap, COLORREF color,HRGN* pRgn);
	BOOL SetBackgroundBitmap(HBITMAP hBitmap,COLORREF clrTransparent);
	BOOL SetBackgroundBitmap(CBitmap *pBitmap,COLORREF clrTransparent);
private:
	HDC   m_hDC;			// background bitmap dc

	DWORD m_dwWidth;		// mask width
	DWORD m_dwHeight;		// mask height

	DWORD	m_dwFlags;		// flags
	POINT	m_MousePoint;	// Mouse position for dragging
	BOOL    m_bInit;        // OnInitDialog has been called.

	HBITMAP  m_hBitmap;
	COLORREF m_clrTransparent;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRgnDialog)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRgnDialog)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RGNDIALOG_H__B66D3B02_85AE_41CB_9531_8528481F6428__INCLUDED_)
