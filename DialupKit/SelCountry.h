#if !defined(AFX_SELCOUNTRY_H__43840720_3A9E_4D4F_BC7B_74219C89E475__INCLUDED_)
#define AFX_SELCOUNTRY_H__43840720_3A9E_4D4F_BC7B_74219C89E475__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelCountry.h : header file
//
#include "DialAPNDB.h"
//#include "DialupKit.h"
//#include "common.h"
//#include "controldata.h"
//#include "Dialsetdlg.h"
//#include "stdafx.h"

typedef enum
{
    APN_DEFAULT = 0X00,
    APN_SPECAIL   
}TypeAPN;

/////////////////////////////////////////////////////////////////////////////
// SelCountry dialog

class SelCountry : public CDialog
{
// Construction
public:
	//_ConnectionPtr m_pConnection;
	SelCountry(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SelCountry)
	enum { IDD = IDD_DIA_INITSET };
	CComboBox	m_hCountry;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SelCountry)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL AddCountry();
	void InitDialApn(TypeAPN);
	void TransDlg();

	// Generated message map functions
	//{{AFX_MSG(SelCountry)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELCOUNTRY_H__43840720_3A9E_4D4F_BC7B_74219C89E475__INCLUDED_)
