
#ifndef __SINGLETON__H__
#define __SINGLETON__H__

// 单件类宏(在应用程序中只保留一份实例)
#include <vector>

#define DEFINE_SINGLETON(cls)                                   \
private:                                                        \
    static std::auto_ptr < cls > m_pInstance;                   \
protected:                                                      \
    cls() {}                                                    \
public:                                                         \
    ~cls() {}                                                   \
    static cls* Instance()                                      \
    {                                                           \
        if(!m_pInstance.get())                                  \
        {                                                       \
            m_pInstance = std::auto_ptr < cls >(new cls());     \
        }                                                       \
        return m_pInstance.get();                               \
    }

#define IMPLEMENT_SINGLETON(cls)                                \
std::auto_ptr < cls > cls::m_pInstance(NULL);

#endif