// StringResource.cpp : Defines the entry point for the DLL application.
//
#define _CRTDBG_MAP_ALLOC

#include "stdafx.h"
#include "StringResource.h"
#include "PCSuiteRes.h"
#include "ResConverion.h"

#include <stdlib.h>
#include <crtdbg.h>

//Check memory leak(keep the definition and inlcude sequence)
//#define _CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h>

CResConverion  *g_pResConverion = NULL;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
			if(g_pResConverion == NULL)
			{
				g_pResConverion = new CResConverion();
			}
			break;

		case DLL_THREAD_DETACH:
			break;

		case DLL_PROCESS_DETACH:
			if(g_pResConverion != NULL)
			{
				delete g_pResConverion;
				g_pResConverion = NULL;
			}
			_CrtDumpMemoryLeaks();
			break;
    }
    return TRUE;
}


STRINGRESOURCE_API int GetStringResource(unsigned int nResID,WCHAR** ppszText)
{
	if(g_pResConverion != NULL)
	{
		return g_pResConverion->GetString(nResID,ppszText);
	}
	else
	{
		return ERR_FAILED;
	}
}

STRINGRESOURCE_API int SetResourceLanguage(TLanguage Language)
{
	if(g_pResConverion != NULL)
	{
		return g_pResConverion->SetLanguage(Language);
	}
	else
	{
		return ERR_FAILED;
	}
}

STRINGRESOURCE_API void FreeStringResource(void* pMemory)
{
	if(pMemory != NULL)
	{
		free(pMemory);
	}
}