//-------------------------------------------------------------------------------------
//          Copyright  2005-2007 AMOI                                         
//                All Rights Reserved.                                                 
// Author:     Xia Zhongsheng                                                                      
// Log:   Author             Date             Description                              
// ------------------------------------------------------------------------------------
//        Xia Zhongsheng        2007-08-10       Generate source file frame                                
//-------------------------------------------------------------------------------------

#define UNICODE

#include "afx.h"
#include "difxapi.h"
#include "wchar.h"
#include "ks.h"

#define MAX_STRING (1024)

#define CHECK_CONDITION(_Condition_, _result_) \
	if ((_Condition_)) \
	{ \
		result = _result_; \
		goto Exit; \
	}

typedef int (*DRIVERFUNC) (const WCHAR *pInfFile);

// Acturally install code of drive package
int InstallAndPreInstallDriverPackage(const WCHAR *pInfFile)
{
	DWORD result = ERROR_SUCCESS;
	DWORD Flags = DRIVER_PACKAGE_LEGACY_MODE | DRIVER_PACKAGE_FORCE;
	INSTALLERINFO_W *pAppInfo = NULL;     // No application association
	BOOL NeedReboot = FALSE;
	
	wprintf(L"\nTry to install dirver %s...", pInfFile);
	result = DriverPackageInstallW(pInfFile, Flags, pAppInfo, &NeedReboot);
	(result == ERROR_NO_SUCH_DEVINST) ? (result = 0) : 0;
	if (result != ERROR_SUCCESS)
	{
		wprintf(L"\nInstall failed, code %x, try preinstall...", result);
		result = DriverPackagePreinstallW(pInfFile, Flags);
		if (result != ERROR_SUCCESS)
		{
			wprintf(L"\nPreinstall failed, code %d", result);
		}
		else
		{
			wprintf(L"\nOK");
		}
	}
	else
	{
		wprintf(L"\nOK");
	}

	return result;
}

// Acturally uninstall code of drive package
int UninstallDriverPackage(const WCHAR *pInfFile)
{
	DWORD Flags = DRIVER_PACKAGE_FORCE | DRIVER_PACKAGE_DELETE_FILES;
	INSTALLERINFO_W *pAppInfo = NULL;    
	BOOL NeedReboot = FALSE;
	DWORD result = ERROR_SUCCESS;

	wprintf(L"\nTry to uninstall file %s",  pInfFile);
	result = DriverPackageUninstallW(pInfFile, Flags, pAppInfo, &NeedReboot);
	if (result != ERROR_SUCCESS)
	{
		wprintf(L"\nUninstall failed, code %x", result);
	}
	else
	{
		wprintf(L"\nOK");
	}

	return result;
}

// Install/uninstall all .inf drivers in the current directory
int DriverInstallUninstallEntry(BOOL bInstall)
{
	int result = 0;
	CString CurrentDir;
	CString SearchPattern;
	CString InfFilePath;
	DWORD SizeNeeded = 0;
	WIN32_FIND_DATAW FindFileData = {0};
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DRIVERFUNC fnDriver = (bInstall) ? InstallAndPreInstallDriverPackage : UninstallDriverPackage;

	// Get current directory
	SizeNeeded = GetCurrentDirectoryW(MAX_STRING, CurrentDir.GetBuffer(MAX_STRING));
	CHECK_CONDITION(SizeNeeded == 0, -1);
	CurrentDir.GetBufferSetLength(SizeNeeded);

	// Compose search pattern
	CurrentDir += L"\\";
	CurrentDir.LockBuffer();
	SearchPattern = CurrentDir;
	SearchPattern += L"*.inf";
	wprintf(L"Executing path %s", SearchPattern);

	// Find files with .inf extension
	hFind = FindFirstFileW(SearchPattern, &FindFileData);
	wprintf(L"\nOpen find %d", hFind);
	CHECK_CONDITION(hFind == INVALID_HANDLE_VALUE, -2);

	// Execute all the .inf file in the dir
	do
	{
		InfFilePath = CurrentDir;
		InfFilePath += FindFileData.cFileName;
		result = fnDriver((LPCTSTR)InfFilePath);
		CHECK_CONDITION(result != 0, result);
	}	
	while (FindNextFileW(hFind, &FindFileData));

Exit:
	if (hFind != INVALID_HANDLE_VALUE)
	{
		FindClose(hFind);
	}
	return result;
}


