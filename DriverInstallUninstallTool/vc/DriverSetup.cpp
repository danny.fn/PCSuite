// DriverSetup.cpp : Defines the entry point for the console application.
//

#include "windows.h"

int DriverInstallUninstallEntry(BOOL bInstall);

int main(int argc, char* argv[])
{
	if(argc > 1
		&& stricmp(argv[1], "/u") ==0)   
	{   
		return DriverInstallUninstallEntry(FALSE);   
	}
	
	return DriverInstallUninstallEntry(TRUE);   
}

