// DriverSetup.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "difxapi.h"
#include "devioctl.h"
#include "ntddscsi.h"
#include "spti.h"
#include "wchar.h"
#include <stdio.h>
#include <stddef.h>
#include <atlstr.h>
#include "atlbase.h"
#include "SetupAPI.h"
#include <devguid.h>

#define MAX_STRING (1024)
#define AMOI_MOBILE_MODEM	L"INQ1 USB Modem"

#define CHECK_CONDITION(_Condition_, _result_) \
	if ((_Condition_)) \
	{ \
		result = _result_; \
		goto Exit; \
	}

typedef int (*DRIVERFUNC) (const WCHAR *pInfFile);

void FindCDandSendScsi();;

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
    int result = 0;
	size_t nRet = 0;

	//添加当前是否已经切换过的判断
	{
		HDEVINFO		hDevInfo;
		SP_DEVINFO_DATA DeviceInfoData;  
		DWORD	i;

		// Enumerate through all devices in Set.
		DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		hDevInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVCLASS_MODEM, 0, 0,DIGCF_PRESENT);
		if (hDevInfo == INVALID_HANDLE_VALUE)
			return 0;
		
		for (i = 0; SetupDiEnumDeviceInfo(hDevInfo, i,
				&DeviceInfoData); i++)
		{
			DWORD DataT;
			char  buffer[MAX_STRING] = {0};
			DWORD buffersize = sizeof(buffer);
			CString strCom;
				
			if (!SetupDiGetDeviceRegistryProperty(
						hDevInfo,
						&DeviceInfoData,
						SPDRP_FRIENDLYNAME,
						&DataT,
						(PBYTE)buffer,
						buffersize,
						&buffersize))
					continue;
			
			strCom.Format(_T("%s"),buffer);

			if (strCom.Find("INQ1") != -1)
			{
				nRet = 1;
				break;
			}
			
		}
		
		// Cleanup	 
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}

	if (nRet != 0)
		return 0;

	//修改成只是用来查找各个光盘并发送SCSI消息，安装驱动功能消失
	FindCDandSendScsi();

	Sleep(2000);
	return 0;
}

//往指定的盘发送scsi消息
void SendSCSI(char*   Drive)
{
    ULONG length = 0;
    ULONG errorCode = 0;
    ULONG returned = 0;
    ULONG sectorSize = 512;
    BOOL status = 0;
    HANDLE fileHandle = NULL;
    PUCHAR dataBuffer = NULL;
    SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;
    SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER sptdwb;
    DWORD accessMode = 0, shareMode = 0;
	char	fileman[MAX_PATH + 1] = {0};
	char	temp[3] = {0};
	
	strncpy(temp,Drive,2);;
    strcpy(fileman,"\\\\.\\");
	strcat(fileman,temp);

    shareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;  // default
    accessMode = GENERIC_WRITE | GENERIC_READ;       // default
    
    fileHandle = CreateFile(fileman,
        accessMode,
        shareMode,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);
    if (fileHandle == INVALID_HANDLE_VALUE) {
        printf("Error opening Error: %d\n", errorCode = GetLastError());
        return;
    }

    ZeroMemory(&sptwb,sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS));
    sptwb.spt.Length = sizeof(SCSI_PASS_THROUGH);
    sptwb.spt.PathId = 0;
    sptwb.spt.TargetId = 1;
    sptwb.spt.Lun = 0;
    sptwb.spt.CdbLength = CDB6GENERIC_LENGTH;
    sptwb.spt.SenseInfoLength = 24;
    sptwb.spt.DataIn = SCSI_IOCTL_DATA_IN;
    sptwb.spt.DataTransferLength = 0;
    sptwb.spt.TimeOutValue = 2;
    sptwb.spt.DataBufferOffset =
        offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
    sptwb.spt.SenseInfoOffset =
        offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
    sptwb.spt.Cdb[0] = SCSIOP_SEEK;
    length = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);

    status = DeviceIoControl(fileHandle,
        IOCTL_SCSI_PASS_THROUGH,
        &sptwb,
        sizeof(SCSI_PASS_THROUGH),
        &sptwb,
        length,
        &returned,
        FALSE); 
    CloseHandle(fileHandle);
    
    return;
}

//查找到系统中的各个光盘盘符，并发送SCSI消息
void FindCDandSendScsi()
{ 
  DWORD   dwBuffer=256;   
  char   szBuffer[256];   
  CString   csCheckeFolder;

  memset(   szBuffer,   0,   dwBuffer   );   
  int   nSize   =   GetLogicalDriveStrings(   dwBuffer,   szBuffer   );   
  if(   nSize   ==   0   )   
  {   
	 return;   
  }   
       
  char   szTemp[4];   
  memset(   szTemp,   0,   4   );   
  int   j=0;   
  int   nLength=0;       //   有几个盘符   

  for(   int   i=0;   i<nSize;   i++   )   
  {   
	  if(   szBuffer[i]   ==   '\0'   )   
	  {   
		  szTemp[j]='\0';     
		    
		  UINT   nDriver   =   GetDriveType(szTemp);     
		  //   得到驱动器的信息   
		  switch(   nDriver   )   
		  {   
			  case   DRIVE_REMOVABLE:     //   是否是软驱   
			  break;   

			  case   DRIVE_CDROM:   //光驱，发送SCSI消息
				{
					csCheckeFolder.Format(_T("%s%S"),szTemp,L"INQ1_MobileModem");
					if (PathFileExists(csCheckeFolder))
						SendSCSI(szTemp);
				}
			  break;   

			  case   DRIVE_FIXED:   
			  break;   
			    
			  case   DRIVE_REMOTE:   
			  break;   
			  case   DRIVE_RAMDISK:   
			  break;   
		  }   
			  nLength++;   
			  j=0;   
			  continue;   
	  }   
	  szTemp[j]=szBuffer[i];   
	  j++;   
  }   
     
	  return;    
}